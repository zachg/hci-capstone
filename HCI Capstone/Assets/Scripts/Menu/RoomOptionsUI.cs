using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class RoomOptionsUI : MonoBehaviour
{
    [SerializeField] private PhotonView photonView;
    [SerializeField] private RoomOptionsData roomOptionsData;

    [SerializeField] private GameObject openButton;
    [SerializeField] private GameObject closeButton;
    [SerializeField] private GameObject popup;

    [SerializeField] private TMPro.TMP_InputField totalRounds;

    [SerializeField] private TMPro.TMP_InputField numWordOptions;
    [SerializeField] private TMPro.TMP_InputField numHints;

    [SerializeField] private TMPro.TMP_InputField chooseWordTime;
    [SerializeField] private TMPro.TMP_InputField gpsDrawTime;
    [SerializeField] private TMPro.TMP_InputField guessWordTime;

    private void OnEnable()
    {
        if (PhotonNetwork.IsMasterClient)
        {
            totalRounds.text = roomOptionsData.TotalRounds.ToString();
            numWordOptions.text = roomOptionsData.NumWordOptions.ToString();
            numHints.text = roomOptionsData.NumHints.ToString();
            chooseWordTime.text = roomOptionsData.ChooseWordTime.ToString();
            gpsDrawTime.text = roomOptionsData.GPSDrawTime.ToString();
            guessWordTime.text = roomOptionsData.GuessWordTime.ToString();

            photonView.RPC("RPC_SendRoomOptions", RpcTarget.OthersBuffered,
            roomOptionsData.TotalRounds, roomOptionsData.NumWordOptions,
            roomOptionsData.NumHints, roomOptionsData.ChooseWordTime,
            roomOptionsData.GPSDrawTime, roomOptionsData.GuessWordTime);

            openButton.SetActive(true);
        }
    }

    private void OnDisable()
    {
        if (PhotonNetwork.IsMasterClient)
        {
            popup.SetActive(false);
            openButton.SetActive(false);
            closeButton.SetActive(false);
        }
    }

    public void OpenRoomOptions()
    {
        popup.SetActive(true);
        openButton.SetActive(false);
        closeButton.SetActive(true);
    }

    public void CloseRoomOptions()
    {
        photonView.RPC("RPC_SendRoomOptions", RpcTarget.OthersBuffered,
            roomOptionsData.TotalRounds, roomOptionsData.NumWordOptions, 
            roomOptionsData.NumHints, roomOptionsData.ChooseWordTime, 
            roomOptionsData.GPSDrawTime, roomOptionsData.GuessWordTime);

        popup.SetActive(false);
        openButton.SetActive(true);
        closeButton.SetActive(false);
    }

    public void ChangeTotalRounds(string input)
    {
        int number = Mathf.Clamp(int.Parse(input), 0, 20);
        roomOptionsData.TotalRounds = number;
    }

    public void ChangeNumWordOptions(string input)
    {
        int number = Mathf.Clamp(int.Parse(input), 1, 7);
        roomOptionsData.NumWordOptions = number;
    }

    public void ChangeNumHints(string input)
    {
        int number = Mathf.Clamp(int.Parse(input), 0, 5);
        roomOptionsData.NumHints = number;
    }

    public void ChangeChooseWordTime(string input)
    {
        int number = Mathf.Clamp(int.Parse(input), 10, 180);
        roomOptionsData.ChooseWordTime = number;
    }

    public void ChangeGPSDrawTime(string input)
    {
        int number = Mathf.Clamp(int.Parse(input), 60, 900);
        roomOptionsData.GPSDrawTime = number;
    }

    public void ChangeGuessWordTime(string input)
    {
        int number = Mathf.Clamp(int.Parse(input), 10, 300);
        roomOptionsData.GuessWordTime = number;
    }

    [PunRPC]
    private void RPC_SendRoomOptions(int totalRounds, int numWordOptions, int numHints,
        int chooseWordTime, int gpsDrawTime, int guessWordTime)
    {
        Debug.Log("Received room options data.");
        roomOptionsData.TotalRounds = totalRounds;
        roomOptionsData.NumWordOptions = numWordOptions;
        roomOptionsData.NumHints = numHints;
        roomOptionsData.ChooseWordTime = chooseWordTime;
        roomOptionsData.GPSDrawTime = gpsDrawTime;
        roomOptionsData.GuessWordTime = guessWordTime;
    }
}
