using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Photon.Pun;

public class Username : MonoBehaviour
{
    [SerializeField] private TMP_InputField inputField;
    [SerializeField] private GameObject UsernamePage;
    [SerializeField] private ConnectToServer loading;
    [SerializeField] private MainMenu mainmenu;
    [SerializeField] private CreateAndJoin createAndJoin;
    // Start is called before the first frame update

    public void SaveUsername()
    {
        PhotonNetwork.NickName = inputField.text;
        Debug.Log("username is" + PhotonNetwork.NickName);
        PlayerPrefs.SetString("Username", inputField.text);
        UsernamePage.SetActive(false);
        if (PhotonNetwork.IsConnected)
        {
            createAndJoin.gameObject.SetActive(true);
        }
        else
        {
            loading.gameObject.SetActive(true);
        }
    }

    public void GoBack()
    {
        Debug.Log("start of go back" + PhotonNetwork.NickName);
        gameObject.SetActive(false);
        Debug.Log("middle of go back" + PhotonNetwork.NickName);
        mainmenu.gameObject.SetActive(true);
        Debug.Log("end of go back" + PhotonNetwork.NickName);
    }
}
