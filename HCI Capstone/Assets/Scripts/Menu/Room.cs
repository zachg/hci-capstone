using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Photon.Pun;
using Photon.Realtime;

public class Room : MonoBehaviourPunCallbacks
{
    [SerializeField] private TMP_Text MyRoomName;
    [SerializeField] private string sceneName;
    [SerializeField] private CreateAndJoin createandjoin;
    [SerializeField] private GameObject startGameButton;

    [SerializeField] private GameObject sceneTransitionUI;

    private void OnEnable()
    {
        MyRoomName.text = PlayerPrefs.GetString("roomName");
        if (PhotonNetwork.IsMasterClient)
        {
            startGameButton.SetActive(true);
        }
    }
    private void OnDisable()
    {
        if (PhotonNetwork.IsMasterClient)
        {
            startGameButton.SetActive(false);
        }
    }
    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        if (PhotonNetwork.IsMasterClient)
        {
            startGameButton.SetActive(true);
        }
    }

    public void StartGame()
    {
        gameObject.SetActive(false);
        PhotonNetwork.CurrentRoom.IsOpen = false;
        PhotonNetwork.CurrentRoom.IsVisible = false;
        PhotonNetwork.CurrentRoom.PlayerTtl = 60000;
        photonView.RPC("RPC_StartGame", RpcTarget.Others);
        sceneTransitionUI.SetActive(true);
        DontDestroyOnLoad(sceneTransitionUI);
        PhotonNetwork.LoadLevel(sceneName);
    }

    [PunRPC]
    private void RPC_StartGame()
    {
        sceneTransitionUI.SetActive(true);
        DontDestroyOnLoad(sceneTransitionUI);
    }

    public void Back()
    {
        PhotonNetwork.LeaveRoom();
        gameObject.SetActive(false);
        createandjoin.gameObject.SetActive(true);
    }
}
