using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Instructions : MonoBehaviour
{
    [SerializeField] private GameObject[] instructions;
    private int index = 0;
    public void GetOut()
    {
        instructions[index].SetActive(false);
    }
    public void GetIn()
    {
        index = 0;
        instructions[0].SetActive(true);
    }
    public void NextPage()
    {
        instructions[index].SetActive(false);
        index++;
        instructions[index].SetActive(true);
    }

    public void PreviousPage()
    {
        instructions[index].SetActive(false);
        index--;
        instructions[index].SetActive(true);
    }
}
