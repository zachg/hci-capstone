using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Photon.Realtime;
using Photon.Pun;

public class RoomListing : MonoBehaviour
{
    [SerializeField] private TMP_Text _text;
    [SerializeField] private Room room;
    [SerializeField] private CreateAndJoin create;

    public RoomInfo RoomInfo {get; private set;}

    public void SetRoomInfo(RoomInfo roomInfo)
    {
        RoomInfo = roomInfo;
        _text.text = roomInfo.Name;
    }

    public void click()
    {
        RoomOptions options = new RoomOptions();
        options.MaxPlayers = 4;
        options.PlayerTtl = 300000;
        PhotonNetwork.JoinOrCreateRoom(_text.text, options, TypedLobby.Default);
        PlayerPrefs.SetString("roomName", _text.text);
    }
}
