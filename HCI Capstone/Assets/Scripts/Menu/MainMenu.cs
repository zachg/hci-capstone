using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    [SerializeField] private Username usernamePage;
    [SerializeField] private Instructions instructions;
    public void PlayGame()
    {
        gameObject.SetActive(false);
        usernamePage.gameObject.SetActive(true);
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
