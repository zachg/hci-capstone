using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Realtime;
using TMPro;


public class CreateAndJoin : MonoBehaviourPunCallbacks
{
    [SerializeField] private TMP_InputField input_CreateOrJoin;
    [SerializeField] private Room room;
    [SerializeField] private Username username;
    public void CreateOrJoinRoom()
    {
        RoomOptions options = new RoomOptions();
        options.MaxPlayers = 4;
        PhotonNetwork.JoinOrCreateRoom(input_CreateOrJoin.text, options, TypedLobby.Default);
        PlayerPrefs.SetString("roomName", input_CreateOrJoin.text);
    }

    public override void OnJoinedRoom()
    {
        gameObject.SetActive(false);
        room.gameObject.SetActive(true);
    }

    public void GoBack()
    {
        Debug.Log("start of go back" + PhotonNetwork.NickName);
        gameObject.SetActive(false);
        username.gameObject.SetActive(true);
        Debug.Log("start of go back" + PhotonNetwork.NickName);
    }

    //public override void OnDisconnected(DisconnectCause cause)
    //{
    //    base.OnDisconnected(cause);
    //    PhotonNetwork.ReconnectAndRejoin();
    //}
}
