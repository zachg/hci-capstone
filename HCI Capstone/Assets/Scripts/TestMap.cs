using Mapbox.Unity.Location;
using Mapbox.Unity.Map;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TestMap : MonoBehaviour
{
    public TMPro.TMP_InputField inputField;

    // Start is called before the first frame update
    IEnumerator Start()
    {
        yield return new WaitForSeconds(1);
        inputField.onTouchScreenKeyboardStatusChanged.AddListener(KeyboardStatusChange);
    }

    // Update is called once per frame
    private void Update()
    {
       
    }

    private void KeyboardStatusChange(TouchScreenKeyboard.Status status)
    {
        if (status == TouchScreenKeyboard.Status.Done)
        {
            Debug.Log("Return was pressed on  mobile.");
        }
    }
}
