using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "RoomSettings", menuName = "Fit to Draw")]
public class RoomOptionsData : ScriptableObject
{
    // This scriptable object persists between scenes

    [SerializeField] private int totalRounds = 3;

    [SerializeField] private int numWordOptions = 5;

    [SerializeField] private int numHints = 2;

    [SerializeField] private int chooseWordTime = 60;
    [SerializeField] private int gpsDrawTime = 600;
    [SerializeField] private int guessWordTime = 90;
    // Scoreboard time is internal

    [SerializeField] private List<string> wordBank;
    
    public int TotalRounds { get => totalRounds; set => totalRounds = value; }
    public int NumWordOptions { get => numWordOptions; set => numWordOptions = value; }
    public int NumHints { get => numHints; set => numHints = value; }
    public int ChooseWordTime { get => chooseWordTime; set => chooseWordTime = value; }
    public int GPSDrawTime { get => gpsDrawTime; set => gpsDrawTime = value; }
    public int GuessWordTime { get => guessWordTime; set => guessWordTime = value; }
    public List<string> WordBank { get => wordBank; }
}
