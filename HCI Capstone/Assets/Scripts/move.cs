using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

public class move : MonoBehaviour
{
    [SerializeField] private Vector3 movement = new Vector3(0, 1, 0);
    [SerializeField] private GameObject zach;

    // Start is called before the first frame update
    private void Start()
    {
       zach.SetActive(false); 
        
    }

    // Update is called once per frame
    private void Update()
    {
        transform.position = transform.position + movement * Time.deltaTime;
    }
}
