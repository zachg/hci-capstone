using System.Collections;
using System.Collections.Generic;
using Photon.Realtime;

/// <summary>
/// A list for storing variables tied to a Photon player's unique actorNumber.
/// </summary>
/// <typeparam name="T">Type of variable you would like to store.</typeparam>
public class ActorList<T> : IEnumerable
{
    public struct ActorData<E>
    {
        public int actorNumber;
        public E data;

        public ActorData(int actorNumber, E data)
        {
            this.actorNumber = actorNumber;
            this.data = data;
        }     
    }

    private LinkedList<ActorData<T>> list; // internal linkedlist used to store KeyValuePairs

    /// <summary>
    /// Constructor for ActorList to fill with players. 
    /// Best usage is Actorlist<T> aList = new ActorList<T>(PhotonNetwork.PlayerList);
    /// </summary>
    /// <param name="players">An array of Photon players.</param>
    public ActorList(Player[] players)
    {
        list = new LinkedList<ActorData<T>>();
        
        for (int i = 0; i < players.Length; i++)
        {
            list.AddLast(new ActorData<T>(players[i].ActorNumber, default));
        }
    }

    /// <summary>
    /// Constructor for an empty ActorList.
    /// </summary>
    public ActorList()
    {
        list = new LinkedList<ActorData<T>>();
    }

    /// <summary>
    /// Getter and setter for an ActorList element. 
    /// Uses array access format (e.g. array[number]) to get and set the generic variable
    /// that is paired with a respective actorNumber.
    /// Throws an exception if the actorNumber cannot be found.
    /// </summary>
    /// <param name="actorNumber">The unique actorNumber for the Photon player.</param>
    /// <returns>Generic type object for the variable stored with the actorNumber.</returns>
    public T this[int actorNumber]
    {
        get
        {
            LinkedListNode<ActorData<T>> currNode = list.First;
            while (currNode != null)
            {
                if (currNode.Value.actorNumber == actorNumber)
                {
                    return currNode.Value.data;
                }
                currNode = currNode.Next;
            }
            throw new System.Exception("Actor number could not be found.");
        }
        set
        {
            LinkedListNode<ActorData<T>> currNode = list.First;
            while (currNode != null)
            {
                if (currNode.Value.actorNumber == actorNumber)
                {
                    currNode.Value = new ActorData<T>(actorNumber, value);
                    return;
                }
                currNode = currNode.Next;
            }
            throw new System.Exception("Actor number could not be found.");
        }
    }

    /// <summary>
    /// Adds an actorNumber and generic variable to the ActorList.
    /// </summary>
    /// <param name="actorNumber">The unique actorNumber for the Photon player.</param>
    /// <param name="value">The value of the generic type variable.</param>
    public void Add(int actorNumber, T value)
    {
        list.AddLast(new ActorData<T>(actorNumber, value));
    }

    /// <summary>
    /// Adds an actorNumber to the ActorList. The generic variable is 
    /// either null or the default for the primative type.
    /// </summary>
    /// <param name="actorNumber">The unique actorNumber for the Photon player.</param>
    public void Add(int actorNumber)
    {
        Add(actorNumber, default);
    }

    /// <summary>
    /// Removes an actorNumber entry from the ActorList.
    /// Throws an exception if the actorNumber cannot be found.
    /// </summary>
    /// <param name="actorNumber">The unique actorNumber for the Photon player.</param>
    /// <returns>The ActorData containing the actorNumber and 
    /// generic value that was removed.</returns>
    public ActorData<T> Remove(int actorNumber)
    {
        LinkedListNode<ActorData<T>> currNode = list.First;
        while (currNode != null)
        {
            if (currNode.Value.actorNumber == actorNumber)
            {
                list.Remove(currNode);
                return currNode.Value;
            }
            currNode = currNode.Next;
        }
        throw new System.Exception("Actor number could not be found.");
    }

    /// <summary>
    /// An iterator for the class.
    /// Usage is "foreach (ActorData<T> entryName in actorListName)"
    /// </summary>
    /// <returns>Iterator.</returns>
    public IEnumerator GetEnumerator()
    {
        LinkedListNode<ActorData<T>> currNode = list.First;
        while (currNode != null)
        {
            yield return currNode.Value;
            currNode = currNode.Next;
        }
    }
}
