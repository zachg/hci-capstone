using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Results : MonoBehaviour
{
    [SerializeField] private Canvas resultsCanvas;
    private CanvasGroup uiElement;
    public CanvasGroup firstPlace;
    public CanvasGroup secondPlace;
    public CanvasGroup thirdPlace;
    public CanvasGroup fourthPlace;
    public Button playAgainButton;
    public Button quitButton;
    // Start is called before the first frame update
    void Start()
    {

        //resultsCanvas.gameObject.SetActive(true);

        //Here, the names of each player are taken and sorted based on score
        firstPlace.transform.GetChild(0).gameObject.GetComponent<TMPro.TMP_Text>().text = "John Doe";
        secondPlace.transform.GetChild(0).gameObject.GetComponent<TMPro.TMP_Text>().text = "Jane Doe";
        thirdPlace.transform.GetChild(0).gameObject.GetComponent<TMPro.TMP_Text>().text = "Jim Doe";
        fourthPlace.transform.GetChild(0).gameObject.GetComponent<TMPro.TMP_Text>().text = "Jerry Doe";
        //Then, the names and rankings are faded into the scene.
        StartCoroutine(ShowResults());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public IEnumerator ShowResults()
    {
        yield return new WaitForSeconds(2f);

        //One by one, fade in each ranking.
        uiElement = firstPlace;
        FadeIn();
        yield return new WaitForSeconds(1.5f);

        uiElement = secondPlace;
        FadeIn();
        yield return new WaitForSeconds(1.5f);

        uiElement = thirdPlace;
        FadeIn();
        yield return new WaitForSeconds(1.5f);

        uiElement = fourthPlace;
        FadeIn();
        yield return new WaitForSeconds(1.5f);

        //Enable the buttons.
        playAgainButton.gameObject.SetActive(true);
        quitButton.gameObject.SetActive(true);

        yield return new WaitForEndOfFrame();
    }

    public void OnClickPlayAgainButton()
    {
        //hitting Play Again will start a new game,
        //taking players to the ready room.
    }

    public void OnClickQuitButton()
    {
        //hitting Quit will send the user to the initial scene.
    }

    public void FadeIn()
    {
        StartCoroutine(FadeCanvasGroup(uiElement, uiElement.alpha, 1));
    }
    
    public IEnumerator FadeCanvasGroup(CanvasGroup cg, float start, float end, float lerpTime = 0.5f)
    {
        float _timeStartedLerping = Time.time; //Start time of lerping
        float timeSinceStarted = Time.time - _timeStartedLerping; //Time since lerping started
        float percentageComplete = timeSinceStarted / lerpTime; //Is lerping complete?

        while(true)
        {
            timeSinceStarted = Time.time - _timeStartedLerping;
            percentageComplete = timeSinceStarted / lerpTime;

            float currVal = Mathf.Lerp(start, end, percentageComplete);

            cg.alpha = currVal;

            if (percentageComplete >= 1)
            {
                break;
            }

            yield return new WaitForEndOfFrame();
        }

        print("done");
    }
}
