using UnityEngine;
using Mapbox.Unity.Map;
using UnityEngine.InputSystem;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using System.Collections.Generic;
using Mapbox.Unity.Location;
using Unity.VisualScripting;

public class GPSControl : MonoBehaviour
{
    [SerializeField] private float mapZoom = 20;
    [SerializeField] private float defaultZoom = 100;
    [SerializeField] private float minZoom = 100;
    [SerializeField] private float maxZoom = 800;
    private float currentZoom;

    [SerializeField] private AbstractMap map;
    [SerializeField] private LocationProviderFactory locationProviderFactory;
    [SerializeField] private Camera mainCamera;
    [SerializeField] private Transform avatar;

    private PlayerInput playerInput;

    private InputAction fingerPos1;
    private InputAction fingerContact1;
    private InputAction fingerPos2;
    private InputAction fingerContact2;

    private Coroutine input;
    private Coroutine input2;

    private bool isSecondaryFingerContacting;

    private bool finger1Down;
    private bool finger2Down;

    private List<RaycastResult> finger1Over;
    private List<RaycastResult> finger2Over;

    private Vector3 prevFinger1Pos3D;
    private Vector3 prevFinger2Pos3D;
    private Vector3 prevDistance;
    private List<float> distanceVelocities;
    private readonly float NumDistanceVelocities = 5;

    private Vector3 moveVelocity;

    private bool isCameraLocked;

    private readonly float MoveVelocityMultiplier = 1.3f; // Pascal case is the naming convention for readonly
    private readonly float ZoomVelocityMultiplier = 6.0f;

    public Text debug;
    public Text debug2;

    public bool IsCameraLocked { get => isCameraLocked; set => isCameraLocked = value; }

    private void Start()
    {
        map.SetZoom(mapZoom);

        distanceVelocities = new List<float>();

        isCameraLocked = true;
    }

    private void Awake()
    {
        playerInput = GetComponent<PlayerInput>();

        fingerPos1 = playerInput.actions["PrimaryFingerPosition"];
        fingerContact1 = playerInput.actions["PrimaryFingerContact"];
        fingerPos2 = playerInput.actions["SecondaryFingerPosition"];
        fingerContact2 = playerInput.actions["SecondaryFingerContact"];

        finger1Over = new List<RaycastResult>();
        finger2Over = new List<RaycastResult>();
    }

    private void OnEnable()
    {
        // Want to subscribe to events while the script is active
        fingerContact1.started += StartInput;
        fingerContact1.canceled += EndInput;
    }

    private void OnDisable()
    {
        // Want to unsubscribe from events while the script is inactive
        fingerContact1.started -= StartInput;
        fingerContact1.canceled -= EndInput;
    }

    private void StartInput(InputAction.CallbackContext context)
    {
        finger1Down = true;
        finger2Down = true;
        input = StartCoroutine(Input());
    }

    private void EndInput(InputAction.CallbackContext context)
    {
        StopCoroutine(input);
    }

    private IEnumerator Input()
    {
        yield return null; // need this because the event system registers touching UI after the input
        while (true)// && !EventSystem.current.IsPointerOverGameObject())
        {
            // ensure that they are not touching a UI element to move the map?

#if !UNITY_EDITOR
            if (fingerContact2.ReadValue<float>() == 0)
            {
                Vector3 finger1Pos = this.fingerPos1.ReadValue<Vector2>();

                if (EventSystem.current.IsPointerOverGameObject())
                {
                    PointerEventData pointer = new PointerEventData(EventSystem.current);
                    pointer.position = finger1Pos;

                    finger1Over = new List<RaycastResult>();
                    EventSystem.current.RaycastAll(pointer, finger1Over);

                    //debug.text = "true";
                }

                SingleFingerInput(finger1Pos);
                //finger2Down = true;
            }
            else
            {
                Vector2 finger1Pos = this.fingerPos1.ReadValue<Vector2>();
                Vector2 finger2Pos = this.fingerPos2.ReadValue<Vector2>();

                if (EventSystem.current.IsPointerOverGameObject())
                {
                    PointerEventData pointer1 = new PointerEventData(EventSystem.current);
                    pointer1.position = finger1Pos;

                    finger1Over = new List<RaycastResult>();
                    EventSystem.current.RaycastAll(pointer1, finger1Over);

                    PointerEventData pointer2 = new PointerEventData(EventSystem.current);
                    pointer2.position = finger2Pos;

                    finger2Over = new List<RaycastResult>();
                    EventSystem.current.RaycastAll(pointer2, finger2Over);
                }

                DoubleFingerInput(finger1Pos, finger2Pos);
                //finger1Down = true;
            }
#endif
            yield return null;
        }
    }

    /// <summary>
    /// One finger is touching the screen.
    /// </summary>
    private void SingleFingerInput(Vector2 fingerPos1)
    {

        Vector3 finger1Pos3D = fingerPos1;
        finger1Pos3D.z = mainCamera.transform.position.y;
        try
        {
            finger1Pos3D = mainCamera.ScreenToWorldPoint(finger1Pos3D);
        }
        catch
        {
            finger1Pos3D = Vector3.zero;
        }
        finger1Pos3D -= new Vector3(mainCamera.transform.position.x, 0, mainCamera.transform.position.z);

        if (finger1Down)
        {
            isCameraLocked = false;
            finger1Down = false;
        }
        else
        {
            moveVelocity = (-(finger1Pos3D - prevFinger1Pos3D) / Time.deltaTime) * MoveVelocityMultiplier;
        }

        prevFinger1Pos3D = finger1Pos3D;
    }

    /// <summary>
    /// Two fingers are touching the screen.
    /// </summary>
    private void DoubleFingerInput(Vector3 finger1Pos, Vector3 finger2Pos)
    {
        Vector3 finger1Pos3D = finger1Pos;
        finger1Pos3D.z = mainCamera.transform.position.y;
        finger1Pos3D = mainCamera.ScreenToWorldPoint(finger1Pos3D);
        finger1Pos3D -= new Vector3(mainCamera.transform.position.x, 0, mainCamera.transform.position.z);

        Vector3 finger2Pos3D = finger2Pos;
        finger2Pos3D.z = mainCamera.transform.position.y;
        finger2Pos3D = mainCamera.ScreenToWorldPoint(finger2Pos3D);
        finger2Pos3D -= new Vector3(mainCamera.transform.position.x, 0, mainCamera.transform.position.z);

        if (finger2Down)
        {
            isCameraLocked = false;
            finger2Down = false;

            distanceVelocities.Clear();
            for (int i = 0; i < NumDistanceVelocities; i++)
            {
                distanceVelocities.Add(0);
            }
        }
        else
        {
            Vector3 finger1Vel = -(finger1Pos3D - prevFinger1Pos3D) / Time.deltaTime;
            Vector3 finger2Vel = -(finger2Pos3D - prevFinger2Pos3D) / Time.deltaTime;
            Vector3 distanceVelocity = ((finger2Pos3D - finger1Pos3D) - prevDistance) / Time.deltaTime;

            float dotTwoFingers = Vector3.Dot(finger1Vel.normalized, finger2Vel.normalized);

            if (dotTwoFingers < 0) // either rotation or zoom
            {
                // only rotate if dot product of the movement and the direction between the two fingers is less than  0.707 (45 degrees)
                float dotFingerDir = Vector3.Dot(finger1Vel.normalized, (finger2Pos3D - finger1Pos3D).normalized);
                //debug2.text = Mathf.Abs(dotFingerDir).ToString();
                if (Mathf.Abs(dotFingerDir) < 0.707f)
                {
                    // do rotation
                }

                Vector3 distance = finger2Pos3D - finger1Pos3D;
                float multiplier = Mathf.Clamp01(Mathf.Abs(dotFingerDir) - 0.707f) / 0.293f;

                distanceVelocities.Add(distanceVelocity.magnitude);
                distanceVelocities.RemoveAt(0);

                float averageDistanceVelocity = 0;
                for (int i = 0; i < distanceVelocities.Count; i++)
                {
                    averageDistanceVelocity += distanceVelocities[i];
                }
                averageDistanceVelocity /= distanceVelocities.Count;

                // do zoom
                if (mainCamera.transform.position.y < minZoom)
                {
                    // do not apply negative y movevelocity
                    moveVelocity.y = (distance.magnitude > prevDistance.magnitude ?
                    0 : averageDistanceVelocity) *
                    multiplier * ZoomVelocityMultiplier;
                }
                else if (mainCamera.transform.position.y > maxZoom)
                {
                    // do not apply positive y movevelocity
                    moveVelocity.y = (distance.magnitude > prevDistance.magnitude ?
                    -averageDistanceVelocity : 0) *
                    multiplier * ZoomVelocityMultiplier;
                }
                else
                {
                    // freely apply positive or negative y movevelocity
                    moveVelocity.y = (distance.magnitude > prevDistance.magnitude ?
                    -averageDistanceVelocity : averageDistanceVelocity) *
                    multiplier * ZoomVelocityMultiplier;
                }
            }
        }

        // prev
        prevFinger1Pos3D = finger1Pos3D;
        prevFinger2Pos3D = finger2Pos3D;
        prevDistance = finger2Pos3D - finger1Pos3D;
    }

    public void RecenterCamera()
    {
        isCameraLocked = true;
    }

    public void ResetCameraOnLocation(double latitude, double longitude)
    {
        isCameraLocked = false;
        mainCamera.transform.position = new Vector3(0, defaultZoom, 0);
        map.SetCenterLatitudeLongitude(new Mapbox.Utils.Vector2d(latitude, longitude));
        map.UpdateMap();
    }

    public void ResetCamera()
    {
        isCameraLocked = false;
        mainCamera.transform.position = new Vector3(0, defaultZoom, 0);
        map.SetCenterLatitudeLongitude(locationProviderFactory.DefaultLocationProvider.CurrentLocation.LatitudeLongitude);
        map.UpdateMap();
    }

    // Rotating
    // finger move direction is perpendicular to vector towards the other finger

    // Zooming
    // finger move direction is parallel or negative parallel to vector towards the other finger

    private void Update()
    {
        //debug.text = mainCamera.transform.position.y.ToString();
        if (isCameraLocked)
        {
            mainCamera.transform.position = new Vector3(avatar.position.x, mainCamera.transform.position.y, avatar.position.z);
        }
        else
        {
            if (fingerContact2.ReadValue<float>() != 0) // both fingers touching
            {
                if (EventSystem.current.IsPointerOverGameObject())
                {
                    // check if any gameobject not ignored
                    bool notIgnored1 = false;
                    foreach (RaycastResult result in finger1Over)
                    {
                        if ((result.gameObject.layer >> 7 & 1) == 0)
                        {
                            notIgnored1 = true;
                            break;
                        }
                    }

                    bool notIgnored2 = false;
                    foreach (RaycastResult result in finger2Over)
                    {
                        if ((result.gameObject.layer >> 7 & 1) == 0)
                        {
                            notIgnored2 = true;
                            break;
                        }
                    }

                    if (notIgnored1)
                    {
                        if (notIgnored2)
                        {
                            // slow move
                            SlowDownMove();
                        }

                        // slow zoom
                        SlowDownZoom();
                    }
                    else if (notIgnored2)
                    {
                        // slow zoom
                        SlowDownZoom();
                    }

                    // slow move and zoom
                }
            }
            else if (fingerContact1.ReadValue<float>() != 0) // one finger touching
            {
                if (EventSystem.current.IsPointerOverGameObject())
                {
                    // check if any gameobject not ignored
                    bool notIgnored = false;
                    foreach (RaycastResult result in finger1Over)
                    {
                        if ((result.gameObject.layer >> 7 & 1) == 0)
                        {
                            notIgnored = true;
                            break;
                        }
                    }

                    if (notIgnored)
                    {
                        // slow move
                        SlowDownMove();
                    }
                }

                // slow zoom
                SlowDownZoom();
            }
            else
            {
                // slow move and zoom
                SlowDownMove();

                SlowDownZoom();
            }

            mainCamera.transform.position = new Vector3(mainCamera.transform.position.x + moveVelocity.x * Time.deltaTime,
                        mainCamera.transform.position.y + moveVelocity.y * Time.deltaTime, 
                        mainCamera.transform.position.z + moveVelocity.z * Time.deltaTime);
        }
    }

    private void SlowDownMove()
    {
        // slow down the movement speed
        if (Math.Abs(new Vector3(moveVelocity.x, 0, moveVelocity.z).magnitude) > 0.01f)
        {
            moveVelocity -= new Vector3(moveVelocity.x, 0, moveVelocity.z) * Mathf.Clamp01(5 * Time.deltaTime); // slows down relative to speed?
        }
        else
        {
            moveVelocity = new Vector3(0, moveVelocity.y, 0);
        }
    }

    private void SlowDownZoom()
    {
        if (Math.Abs(new Vector3(0, moveVelocity.y, 0).magnitude) > 0.01f)
        {
            moveVelocity -= new Vector3(0, moveVelocity.y, 0) * Mathf.Clamp01(10 * Time.deltaTime); // slows down relative to speed?
        }
        else
        {
            moveVelocity = new Vector3(moveVelocity.x, 0, moveVelocity.z);
        }
        // if over the max limit the multiplier inverse (100 + maxZoom) - y

        // if the new velocity is opposite dot of the old velocity do zero
    }
}