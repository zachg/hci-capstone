using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Loading : MonoBehaviour
{
    [SerializeField] private string meneSceneName;
    // Have the master client confirm that all players have joined the game before allowing the gamecontroller script to start
    private PhotonView photonView;
    private ActorList<bool> playersFinished;

    [SerializeField] private GameController gameController;

    private void Start()
    {
        photonView = GetComponent<PhotonView>();
        if (!PhotonNetwork.IsConnected)
        {
            SceneManager.LoadScene(meneSceneName);
        }
        else 
        { 
            if (PhotonNetwork.IsMasterClient)
            {
                playersFinished = new ActorList<bool>(PhotonNetwork.PlayerList);
            }
            Finish();
        }
    }

    private void Finish()
    {
        if (PhotonNetwork.IsMasterClient)
        {
            // set your own actornumber to finished
            playersFinished[PhotonNetwork.LocalPlayer.ActorNumber] = true;

            foreach (ActorList<bool>.ActorData<bool> actorElem in playersFinished)
            {
                if (!actorElem.data) return;
            }

            // tell all other clients that this game state is over
            photonView.RPC("RPC_FinishLoading", RpcTarget.Others);
            Destroy(GameObject.Find("Scene Transition UI"));
            gameController.enabled = true;
        }
        else
        {
            // tell the master client to set your actornumber to finished
            photonView.RPC("RPC_StopLoading", RpcTarget.OthersBuffered, PhotonNetwork.LocalPlayer.ActorNumber); // buffered because how do we know if the master client will load in first
        }
    }

    /// <summary>
    /// Tells the master client that another client has finished.
    /// This function will check if all players are finished.
    /// Run by the master client.
    /// </summary>
    /// <param name="actorNumber">The photon actor number of the client that is finished.</param>
    [PunRPC]
    private void RPC_StopLoading(int actorNumber)
    {
        if (PhotonNetwork.IsMasterClient)
        {
            // check the finished states of all the users
            playersFinished[actorNumber] = true;

            foreach (ActorList<bool>.ActorData<bool> actorElem in playersFinished)
            {
                if (!actorElem.data) return;
            }

            // tell all other clients that this game state is over
            photonView.RPC("RPC_FinishLoading", RpcTarget.Others);
            Destroy(GameObject.Find("Scene Transition UI"));
            gameController.enabled = true;
        }
    }

    /// <summary>
    /// Turn on the Gamecontroller and start running the game. Run by the non master clients.
    /// </summary>
    [PunRPC]
    private void RPC_FinishLoading()
    {
        Destroy(GameObject.Find("Scene Transition UI"));
        gameController.enabled = true;
    }
}
