using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Draw : MonoBehaviour
{
    [SerializeField] private RenderTexture drawingRenderTexture;
    private Vector3 drawPosition;

    [SerializeField] private GameObject colorPanel;
    [SerializeField] private GameObject hideColorPanel;

    [SerializeField] private GameObject emptyMarkerIcon;
    [SerializeField] private Marker[] markers;
    [SerializeField] private GameObject fullEraser;
    private Marker activeMarker;

    public Vector3 DrawPosition { set => drawPosition = value; }
    public RenderTexture DrawingRenderTexture => drawingRenderTexture;

    public Marker[] Markers => markers;

    public void SetUp()
    {
        for (int i = 0; i < markers.Length; i++)
        {
            markers[i].OnSelect += SetMarkerActive;
        }
    }

    public void Initialize()
    {
        emptyMarkerIcon.SetActive(true);
        drawingRenderTexture.Release();
    }

    // hide color panel is pressed
    public void OpenColorPanel()
    {
        hideColorPanel.SetActive(false);
        colorPanel.SetActive(true);
    }

    public void SetMarkerActive(Marker marker, bool state)
    {
        if (state)
        {
            activeMarker?.Toggle.SetIsOnWithoutNotify(false);
            activeMarker?.Icon.SetActive(false);
            activeMarker?.MarkerTransform.gameObject.SetActive(false);

            activeMarker = marker;

            marker.MarkerTransform.position = drawPosition;
            marker.Icon.SetActive(true);
            marker.MarkerTransform.gameObject.SetActive(true);
        }
        else
        {
            activeMarker.MarkerTransform.gameObject.SetActive(false);
            activeMarker.Icon.SetActive(false);
            activeMarker = null;
        }

        // close the menu
        hideColorPanel.SetActive(true);
        colorPanel.SetActive(false);

        emptyMarkerIcon.SetActive(!state);
    }

    // Update is called once per frame
    private void Update()
    {
        if (activeMarker)
        {
            activeMarker.MarkerTransform.position = drawPosition;
        }
    }

    public void Finish()
    {
        activeMarker?.Toggle.SetIsOnWithoutNotify(false);
        activeMarker?.MarkerTransform.gameObject.SetActive(false);
        activeMarker?.Icon.SetActive(false);
        activeMarker = null;
    }

    public void ResetCanvas()
    {
        drawingRenderTexture.Release();
        StartCoroutine(FullErase());
    }

    private IEnumerator FullErase()
    {
        fullEraser.SetActive(true);
        yield return null;
        fullEraser.SetActive(false);
    }
}