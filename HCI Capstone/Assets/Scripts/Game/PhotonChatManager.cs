using ExitGames.Client.Photon;
using Photon.Chat;
using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PhotonChatManager : MonoBehaviour, IChatClientListener
{

    ChatClient chatClient;
    bool isConnected;
    [SerializeField] string username;
    [SerializeField] InputField chatField;
    [SerializeField] Text chatDisplay;
    string currentChat;
    [SerializeField] string correctWord;

    // Start is called before the first frame update
    void Start()
    {
        //Set up isConnected and chatClient
        isConnected = true;
        chatClient = new ChatClient(this);

        //Set username for chat.
        //For now, the name is hardcoded as John Doe.

        //Set correct word here.
        //For now, the correct word is hardcoded as "It's a Secret!!!"

        //Connect the chatClient 
        chatClient.Connect(PhotonNetwork.PhotonServerSettings.AppSettings.AppIdChat, PhotonNetwork.AppVersion, new AuthenticationValues(username));
        Debug.Log("Connecting...");
    }

    // Update is called once per frame
    void Update()
    {
        if (isConnected)
        {
            chatClient.Service();
        }

        //If there is something in the chatField and Return is hit,
        //send the message.
        if (chatField.text != "" && Input.GetKey(KeyCode.Return))
        {
            SubmitChatOnEnter();
        }
    }

    public void TypeChatOnValueChange(string valueIn)
    {
        currentChat = valueIn;
    }

    public void SubmitChatOnEnter()
    {
        chatClient.PublishMessage("RegionChannel", currentChat);
        currentChat = "";
        chatField.text = "";
    }

    public void UpdateCorrectWord(string newWord)
    {
        correctWord = newWord;
    }

    public void DebugReturn(DebugLevel level, string message)
    {
        Debug.Log("Debug level " + level + ": " + message);
    }

    public void OnChatStateChange(ChatState state)
    {
        //do nothing???
    }

    public void OnConnected()
    {
        Debug.Log("Connected");
        isConnected = true;
        bool subscribed = chatClient.Subscribe(new string[] { "RegionChannel" });
        if (subscribed)
        {
            Debug.Log("Subscribed to RegionChannel");
        }
        else
        {
            Debug.Log("ERROR: Not subscribed to RegionChannel");
        }
    }

    public void OnDisconnected()
    {
        Debug.Log("Disconnected");
    }

    public void OnGetMessages(string channelName, string[] senders, object[] messages)
    {
        string content = "";
        for (int i = 0; i < senders.Length; i++)
        {
            Debug.Log("Message is: " + messages[i]);
            //For each message, if a user's message equals the correct word,
            //change their message to avoid spoiling the answer.
            if (messages[i].Equals(correctWord))
            {
                Debug.Log("Correct word!");
                content = string.Format("{0} guessed the right word!", senders[i]);
            }
            else
            {
                Debug.Log("Normal word");
                content = string.Format("{0}: {1}", senders[i], messages[i]);
            }
            chatDisplay.text += "\n " + content;

            Debug.Log(content);
        }
    }

    public void OnPrivateMessage(string sender, object message, string channelName)
    {
        //No private messages needed, so not implemeneted.
        throw new System.NotImplementedException();
    }

    public void OnStatusUpdate(string user, int status, bool gotMessage, object message)
    {
        throw new System.NotImplementedException();
    }

    public void OnSubscribed(string[] channels, bool[] results)
    {
        //do nothing I guess
    }

    public void OnUnsubscribed(string[] channels)
    {
        throw new System.NotImplementedException();
    }

    public void OnUserSubscribed(string channel, string user)
    {
        throw new System.NotImplementedException();
    }

    public void OnUserUnsubscribed(string channel, string user)
    {
        throw new System.NotImplementedException();
    }
}
