using Mapbox.Unity.Location;
using Mapbox.Unity.Map;
using Photon.Pun;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GPSDraw : MonoBehaviour
{
    private PhotonView photonView; // needed to do RPCs

    [SerializeField] private GPSControl gpsControl;
    [SerializeField] private Draw draw; // script managing drawing
    private double canvasLatitude; // center latitude of the drawing canvas
    private double canvasLongitude; // center longitude of the drawing canvas

    [SerializeField] private AbstractMap map; // mapbox map

    [SerializeField] private Transform userAvatar; // icon used to mark the position of the user on the GPS

    [SerializeField] private ChooseWord chooseWord;

    private ActorList<bool> playersReady; // a list of actors and boolean pairs to mark who is ready
    private ActorList<bool> playersFinished; // a list of actors and boolean pairs to mark who is finished

    private Coroutine currentTimer; // current timer coroutine

    private readonly int PreTimerPeriod = 1; // number of seconds for the pre drawing timer
    private int timerPeriod; // number of seconds for the drawing timer

    // pre timer window UI
    [SerializeField] private GameObject preDrawUI;

    [SerializeField] private GameObject drawUI;
    [SerializeField] private TMPro.TMP_Text timerText; // timer text field

    [SerializeField] private GameObject waitingUI; // UI for waiting for all players to finsh drawing

    [SerializeField] private GameObject wordDisplay;
    [SerializeField] private GameObject showWordButton;
    [SerializeField] private GameObject hideWordButton;

    [SerializeField] private GameObject errorMessage;

    public Action OnFinished;

    public double CanvasLatitude => canvasLatitude;
    public double CanvasLongitude => canvasLongitude;

    /// <summary>
    /// Sets up the GPSDraw script. Called in the start function of GameController.
    /// </summary>
    /// <param name="roomSettings">The RoomSettingsData scriptable object containing our times.</param>
    public void SetUp(RoomOptionsData roomSettings)
    {
        photonView = GetComponent<PhotonView>();
        timerPeriod = roomSettings.GPSDrawTime;
        draw.SetUp();
    }

    /// <summary>
    /// Begins the drawing phase of the game.
    /// </summary>
    public void Initialize()
    {
        preDrawUI.SetActive(true); // turn on UI pre timer group
        wordDisplay.transform.GetChild(0).GetComponent<TMPro.TMP_Text>().text = chooseWord.ChosenWords[PhotonNetwork.LocalPlayer.ActorNumber];
        draw.Initialize();

        gpsControl.RecenterCamera();
        userAvatar.gameObject.SetActive(true);

        showWordButton.SetActive(true);
        hideWordButton.SetActive(false);
        wordDisplay.SetActive(false);

        // since the has been called, remove this function from the onclick event
        for (int i = 0; i < draw.Markers.Length; i++)
        {
            draw.Markers[i].Toggle.onValueChanged.AddListener(PlaceGPSCanvas);
        }

        if (PhotonNetwork.IsMasterClient)
        {
            if (playersReady == null) playersReady = new ActorList<bool>(PhotonNetwork.PlayerList);
            playersReady[PhotonNetwork.LocalPlayer.ActorNumber] = true;

            // check if all the players are ready
            foreach (ActorList<bool>.ActorData<bool> actorElem in playersReady)
            {
                if (!actorElem.data) return;
            }

            photonView.RPC("RPC_InitializeContinueDraw", RpcTarget.Others);
            currentTimer = StartCoroutine(PreTimer());

            playersFinished = new ActorList<bool>(PhotonNetwork.PlayerList);
        }
        else
        {
            photonView.RPC("RPC_InitializeDraw", RpcTarget.MasterClient, PhotonNetwork.LocalPlayer.ActorNumber);
        }
    }

    [PunRPC]
    private void RPC_InitializeDraw(int actorNumber)
    {
        if (playersReady == null) playersReady = new ActorList<bool>(PhotonNetwork.PlayerList);
        playersReady[actorNumber] = true;

        // check if all the players are ready
        foreach (ActorList<bool>.ActorData<bool> actorElem in playersReady)
        {
            if (!actorElem.data) return;
        }

        photonView.RPC("RPC_InitializeContinueDraw", RpcTarget.Others);
        currentTimer = StartCoroutine(PreTimer());

        playersFinished = new ActorList<bool>(PhotonNetwork.PlayerList);
    }

    [PunRPC]
    private void RPC_InitializeContinueDraw()
    {
        currentTimer = StartCoroutine(PreTimer());
    }

    /// <summary>
    /// The timer before the drawing phase starts.
    /// </summary>
    /// <returns>Coroutine waits 1 second intervals.</returns>
    private IEnumerator PreTimer()
    {
        for (int i = PreTimerPeriod; i > 0; i--)
        {
            yield return new WaitForSeconds(1f);
        }
        StartDrawing();
    }

    /// <summary>
    /// Signals when the player can start drawing. Turns off the pre drawing UI and turns on the drawing UI.
    /// </summary>
    private void StartDrawing()
    {
        currentTimer = StartCoroutine(Timer());

        preDrawUI.SetActive(false); // turn on UI pre timer group
        drawUI.SetActive(true); // turn on UI drawing group

        // TODO set the drawing stuff active
    }

    /// <summary>
    /// Timer for the period of time the user has to draw their picture.
    /// </summary>
    /// <returns>Coroutine waits 1 second intervals.</returns>
    private IEnumerator Timer()
    {
        for (int i = timerPeriod; i > 0; i--)
        {
            timerText.text = i.ToString();
            yield return new WaitForSeconds(1f);
        }
        StopDrawing();
    }

    public void ShowWord()
    {
        showWordButton.SetActive(false);
        hideWordButton.SetActive(true);

        wordDisplay.SetActive(true);
    }

    public void HideWord()
    {
        showWordButton.SetActive(true);
        hideWordButton.SetActive(false);

        wordDisplay.SetActive(false);
    }

    /// <summary>
    /// Can either be triggered by the timer running out or the done button being pressed.
    /// </summary>
    public void StopDrawing()
    {
        // set the drawing stuff inactive
        draw.Finish();
        draw.gameObject.SetActive(false);

        if (currentTimer != null)  StopCoroutine(currentTimer); // TODO may need to check null?

        // TODO show waiting screen in the meantime
        drawUI.SetActive(false);
        userAvatar.gameObject.SetActive(false);
        waitingUI.SetActive(true);

        if (PhotonNetwork.IsMasterClient)
        {
            // set your own actornumber to finished
            playersFinished[PhotonNetwork.LocalPlayer.ActorNumber] = true;

            foreach (ActorList<bool>.ActorData<bool> actorElem in playersFinished)
            {
                if (!actorElem.data) return;
            }

            // tell all other clients that this game state is over
            photonView.RPC("RPC_FinishGPSDraw", RpcTarget.Others);
            playersReady = null;
            playersFinished = null;
            waitingUI.SetActive(false);
            OnFinished();
        }
        else
        {
            // tell the master client to set your actornumber to finished
            photonView.RPC("RPC_StopDrawing", RpcTarget.MasterClient, PhotonNetwork.LocalPlayer.ActorNumber);
        }
    }

    /// <summary>
    /// Tells the master client that another client has finished drawing.
    /// This function will check if all players are finished.
    /// Run by the master client.
    /// </summary>
    /// <param name="actorNumber">The photon actor number of the client that is finished.</param>
    [PunRPC]
    private void RPC_StopDrawing(int actorNumber)
    {
        try
        {
            // check the finished states of all the users
            playersFinished[actorNumber] = true;
        }
        catch
        {
            Debug.LogError("A player disconnected and the new master client received an RPC before the previous master client rejoined. This error is not recoverable");
            errorMessage.SetActive(true);
            return;
        }

        foreach (ActorList<bool>.ActorData<bool> actorElem in playersFinished)
        {
            if (!actorElem.data) return;
        }

        // tell all other clients that this game state is over
        photonView.RPC("RPC_FinishGPSDraw", RpcTarget.Others);
        playersReady = null;
        playersFinished = null;
        waitingUI.SetActive(false); // turn off the waiting UI
        OnFinished();
    }

    /// <summary>
    /// Call the OnFinished action. Run by the non master clients.
    /// </summary>
    [PunRPC]
    private void RPC_FinishGPSDraw()
    {
        waitingUI.SetActive(false); // turn off the waiting UI
        OnFinished();
    }

    // place drawing canvas on map

    // start GPS drawing
    // position the drawing canvas

    /// <summary>
    /// Places the canvas at the point where the user first starts drawing.
    /// </summary>
    /// <param name="state">We don't care about the state, but this is necessary for the toggle onValueChanged event.</param>
    private void PlaceGPSCanvas(bool state)
    {
        Mapbox.Utils.Vector2d latlong = map.WorldToGeoPosition(userAvatar.transform.position);
        canvasLatitude = latlong.x;
        canvasLongitude = latlong.y;

        draw.transform.position = new Vector3(userAvatar.position.x, 0.5f, userAvatar.position.z);
        draw.gameObject.SetActive(true);

        // since the has been called, remove this function from the onclick event
        for (int i = 0; i < draw.Markers.Length; i++)
        {
            draw.Markers[i].Toggle.onValueChanged.RemoveListener(PlaceGPSCanvas);
        }
        draw.ResetCanvas();
        //StartCoroutine(FixCanvas());
    }

    private IEnumerator FixCanvas()
    {
        yield return null;
        draw.ResetCanvas();
    }

    public void ShowGPSCanvas()
    {
        draw.transform.position = new Vector3(0, 0.5f, 0);
        draw.gameObject.SetActive(true);
    }

    public void HideGPSCanvas()
    {
        draw.gameObject.SetActive(false);
    }

    // end GPS drawing
    // save GPS drawing

    /// <summary>
    /// Runs once each frame.
    /// </summary>
    private void Update()
    {
        draw.DrawPosition = new Vector3(userAvatar.position.x, 1f, userAvatar.position.z);
    }

    // TODO maybe find the bounds of where the user drew
}