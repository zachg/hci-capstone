using Photon.Pun;
using System;
using System.Collections;
using UnityEngine;

public class Scoreboard : MonoBehaviour
{
    public struct Score
    {
        // do we want one or two variables for score?
        // probably want a "+100 points"
        public int pointsThisRound;
        public int totalPoints;
    }

    private PhotonView photonView;

    private ActorList<Score> scores;
    private ActorList<bool> playersFinished;

    [SerializeField] private Transform scrollableListGridContent;
    [SerializeField] private GameObject scoreTextPrefab;
    private TMPro.TMP_Text[] totalScoresText;
    private TMPro.TMP_Text[] addedScoresText;

    [SerializeField] private int PreTimerPeriod = 10; // number of seconds for the pre scoreboard timer
    [SerializeField] private int TimerPeriod = 20;

    [SerializeField] private GameObject preScoreUI;

    [SerializeField] private GameObject scoreUI;
    [SerializeField] private TMPro.TMP_Text timerText; // timer text field

    public Action OnFinished;

    private CanvasGroup uiElement;

    public ActorList<Score> Scores => scores;

    public void SetUp()
    {
        photonView = GetComponent<PhotonView>();
        scores = new ActorList<Score>(PhotonNetwork.PlayerList);
        CreateScoreText();
    }

    private void CreateScoreText()
    {
        totalScoresText = new TMPro.TMP_Text[PhotonNetwork.PlayerList.Length]; // array of size playerlist
        addedScoresText = new TMPro.TMP_Text[PhotonNetwork.PlayerList.Length]; // array of size playerlist

        for (int i = 0; i < PhotonNetwork.PlayerList.Length; i++)
        {
            GameObject newScoreText = Instantiate(scoreTextPrefab, scrollableListGridContent); // makes the new gameobject a child of the scrollableListGridContent transform
            newScoreText.transform.GetChild(0).GetComponent<TMPro.TMP_Text>().text = PhotonNetwork.PlayerList[i].NickName;
            totalScoresText[i] = newScoreText.transform.GetChild(1).GetComponent<TMPro.TMP_Text>();
            addedScoresText[i] = newScoreText.transform.GetChild(2).GetComponent<TMPro.TMP_Text>();
        }
    }

    public void Initialize()
    {
        StartCoroutine(PreTimer());

        if (PhotonNetwork.IsMasterClient)
        {
            playersFinished = new ActorList<bool>(PhotonNetwork.PlayerList);
        }

        preScoreUI.SetActive(true);

        int i = 0;
        foreach (ActorList<Score>.ActorData<Score> score in scores)
        {
            totalScoresText[i].text = score.data.totalPoints.ToString();
            addedScoresText[i].text = "+" + score.data.pointsThisRound.ToString();
            i++;
        }
    }

    /// <summary>
    /// The timer before the drawing phase starts.
    /// </summary>
    /// <returns>Coroutine waits 1 second intervals.</returns>
    private IEnumerator PreTimer()
    {
        for (int i = PreTimerPeriod; i > 0; i--)
        {
            yield return new WaitForSeconds(1f);
        }
        DisplayScoreboard();
    }

    private void DisplayScoreboard()
    {
        StartCoroutine(Timer());
        preScoreUI.SetActive(false);
        scoreUI.SetActive(true);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns>Coroutine waits 1 second intervals.</returns>
    private IEnumerator Timer()
    {
        for (int i = TimerPeriod; i > 0; i--)
        {
            timerText.text = i.ToString();
            yield return new WaitForSeconds(1f);
        }
        Finish();
    }

    private void Finish()
    {
        if (PhotonNetwork.IsMasterClient)
        {
            // set your own actornumber to finished
            playersFinished[PhotonNetwork.LocalPlayer.ActorNumber] = true;

            foreach (ActorList<bool>.ActorData<bool> actorElem in playersFinished)
            {
                if (!actorElem.data) return;
            }

            // tell all other clients that this game state is over
            photonView.RPC("RPC_FinishScores", RpcTarget.Others);
            AddScores();
            scoreUI.SetActive(false); // turn off the score UI
            OnFinished();
        }
        else
        {
            // tell the master client to set your actornumber to finished
            photonView.RPC("RPC_StopScores", RpcTarget.MasterClient, PhotonNetwork.LocalPlayer.ActorNumber);
        }

        //if (currRound == totalRounds)
        //{
        //  make any necessary preparations to show results screen
        //  ShowFinalScores()
        //}
    }

    /// <summary>
    /// Tells the master client that another client has finished.
    /// This function will check if all players are finished.
    /// Run by the master client.
    /// </summary>
    /// <param name="actorNumber">The photon actor number of the client that is finished.</param>
    [PunRPC]
    private void RPC_StopScores(int actorNumber)
    {
        // check the finished states of all the users
        playersFinished[actorNumber] = true;

        foreach (ActorList<bool>.ActorData<bool> actorElem in playersFinished)
        {
            if (!actorElem.data) return;
        }

        // tell all other clients that this game state is over
        photonView.RPC("RPC_FinishScores", RpcTarget.Others);
        AddScores();
        scoreUI.SetActive(false); // turn off the score UI
        OnFinished();
    }

    /// <summary>
    /// Call the OnFinished action. Run by the non master clients.
    /// </summary>
    [PunRPC]
    private void RPC_FinishScores()
    {
        AddScores();
        scoreUI.SetActive(false); // turn off the score UI
        OnFinished();
    }

    private void AddScores()
    {
        for (int i = 0; i < PhotonNetwork.PlayerList.Length; i++)
        {
            Score score = scores[PhotonNetwork.PlayerList[i].ActorNumber];
            score.totalPoints += score.pointsThisRound;
            score.pointsThisRound = 0;
            scores[PhotonNetwork.PlayerList[i].ActorNumber] = score;
        }
    }
    
    //This will get called after all rounds have passed
    public void ShowFinalScores()
    {
        //Set the canvas for the results phase as active, deactivate everything else
        GameObject.Find("ResultsCanvas").gameObject.SetActive(true);
        preScoreUI.gameObject.SetActive(false);
        scoreUI.gameObject.SetActive(false);

        //For the number of players, add their names
        //to each place for 1st thru 4th place.
        int numPlayers = PhotonNetwork.PlayerList.Length;

        //While there logically needs to be at least one player in the game,
        //it's better to be safe than sorry and check for this.
        if (numPlayers >= 1)
        {
            GameObject.Find("FirstPlaceText").GetComponent<TMPro.TMP_Text>().text = PhotonNetwork.PlayerList[1].NickName;
        }
        
        if (numPlayers >= 2)
        {
            GameObject.Find("SecondPlaceText").GetComponent<TMPro.TMP_Text>().text = PhotonNetwork.PlayerList[2].NickName;
        }

        if (numPlayers >= 3)
        {
            GameObject.Find("ThirdPlaceText").GetComponent<TMPro.TMP_Text>().text = PhotonNetwork.PlayerList[3].NickName;
        }

        if (numPlayers >= 4)
        {
            GameObject.Find("FourthPlaceText").GetComponent<TMPro.TMP_Text>().text = PhotonNetwork.PlayerList[4].NickName;
        }

        //Start the fade-in coroutine
        StartCoroutine(ShowResults(numPlayers));
    }

    public IEnumerator ShowResults(int numPlayers)
    {
        //Wait two seconds for players to adjust
        yield return new WaitForSeconds(2f);

        //One by one, fade in each ranking.
        if (numPlayers >= 1)
        {
            uiElement = GameObject.Find("FirstPlace").GetComponent<CanvasGroup>();
            FadeIn();
            yield return new WaitForSeconds(1.5f);
        }

        if (numPlayers >= 2)
        {
            uiElement = GameObject.Find("SecondPlace").GetComponent<CanvasGroup>();
            FadeIn();
            yield return new WaitForSeconds(1.5f);
        }

        if (numPlayers >= 3)
        {
            uiElement = GameObject.Find("ThirdPlace").GetComponent<CanvasGroup>();
            FadeIn();
            yield return new WaitForSeconds(1.5f);
        }

        if (numPlayers >= 4)
        {
            uiElement = GameObject.Find("FourthPlace").GetComponent<CanvasGroup>();
            FadeIn();
            yield return new WaitForSeconds(1.5f);
        }

        //Enable the button.
        GameObject.Find("QuitButton").gameObject.SetActive(true);

        yield return new WaitForEndOfFrame();
    }

    public void OnClickQuitButton()
    {
        //hitting Quit will send the user to the initial scene.

        //LoadScene(Priya's Scene);
    }

    public void FadeIn()
    {
        StartCoroutine(FadeCanvasGroup(uiElement, uiElement.alpha, 1));
    }

    public IEnumerator FadeCanvasGroup(CanvasGroup cg, float start, float end, float lerpTime = 0.5f)
    {
        float _timeStartedLerping = Time.time; //Start time of lerping
        float timeSinceStarted = Time.time - _timeStartedLerping; //Time since lerping started
        float percentageComplete = timeSinceStarted / lerpTime; //Is lerping complete?

        while (true)
        {
            timeSinceStarted = Time.time - _timeStartedLerping;
            percentageComplete = timeSinceStarted / lerpTime;

            float currVal = Mathf.Lerp(start, end, percentageComplete);

            cg.alpha = currVal;

            if (percentageComplete >= 1)
            {
                break;
            }

            yield return new WaitForEndOfFrame();
        }

        print("done");
    }
}
