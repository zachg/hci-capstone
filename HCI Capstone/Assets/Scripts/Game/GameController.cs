using Photon.Pun;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Loading script runs before this to ensure general synchronocity of Start().
/// </summary>
public class GameController : MonoBehaviour
{
    public enum GameState
    {
        choose, // players
        draw,
        guess,
        score
    }

    private GameState gameState;

    [SerializeField] private RoomOptionsData roomSettings;

    private int currentRound = 1;

    [SerializeField] private ChooseWord choosing;
    [SerializeField] private GPSDraw drawing;
    [SerializeField] private Guess guessing;
    [SerializeField] private Scoreboard scoring;

    [SerializeField] private GameObject error;

    private void Start()
    {
        // load the other values from the scriptable object

        // set up each GameState class
        choosing.SetUp(roomSettings);
        drawing.SetUp(roomSettings);
        guessing.SetUp(roomSettings);
        scoring.SetUp();

        choosing.OnFinished += () => ChangeState(GameState.draw);
        drawing.OnFinished += () => ChangeState(GameState.guess); 
        guessing.OnFinished += () => ChangeState(GameState.score);
        scoring.OnFinished += () =>
        {
            if (currentRound == roomSettings.TotalRounds)
            {
                // end the game? 
                // podium screen
            }
            else
            {
                currentRound++;
                ChangeState(GameState.choose);
            }
        };

        // subscribe myWord to chooseword

        ChangeState(GameState.choose); // start the game with the choose state
    }

    //state machine for different game states?
    private void ChangeState(GameState newGameState)
    {
        //try
        //{
        gameState = newGameState;
        switch (gameState)
        {
            case GameState.choose:
                choosing.Initialize();
                break;
            case GameState.draw:
                drawing.Initialize();
                break;
            case GameState.guess:
                guessing.Initialize();
                break;
            case GameState.score:
                scoring.Initialize();
                break;
            default:
                throw new System.Exception("Invalid GameState value");
        }
        //}
        //catch (Exception e)
        //{
        //    error.GetComponent<TMPro.TMP_Text>().text = e.Message;
        //    error.SetActive(true);
        //}
    }
}
