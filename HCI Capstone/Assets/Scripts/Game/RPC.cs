using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RPC : MonoBehaviour
{
    [SerializeField] private PhotonView photonView;

    private Hello hello;

    // Start is called before the first frame update
    private void Start()
    {
        if (PhotonNetwork.IsMasterClient)
        {
            MasterClientFunc();
        }
        else
        {
            ClientFunc();
        }
    }

    private TMPro.TMP_InputField inputField;
    private Text text;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Return))
        {
            SendMessage();
        }
    }

    private void SendMessage()
    {
        string word = inputField.text;

        photonView.RPC("RPC_VerifyWord", RpcTarget.MasterClient, word);
    }

    private string correctWord;

    [PunRPC]
    private void RPC_VerifyWord(string word)
    {
        if (word.Equals(correctWord))
        {
            text.text = "Player guessed the word.";
            photonView.RPC("RPC_GuessedWord", RpcTarget.Others);
        }
        else
        {
            text.text = word;
            photonView.RPC("RPC_NotGuessedWord", RpcTarget.Others, word);
        }
    }

    [PunRPC]
    private void RPC_GuessedWord()
    {
        text.text = "Player guessed the word.";
    }

    [PunRPC]
    private void RPC_NotGuessedWord(string word)
    {
        text.text = word;
    }

    private void MasterClientFunc()
    {

    }

    private void ClientFunc()
    {

    }

    private bool[] playersReady;
    private int index;

    public void PlayerReady(bool state)
    {
        if (PhotonNetwork.IsMasterClient)
        {
            playersReady[index] = state;

            for (int i = 0; i < playersReady.Length; i++)
            {
                if (!playersReady[i])
                {
                    return;
                }
            }

            // load the game scene
        }
        else
        {
            photonView.RPC("RPC_PlayerReady", RpcTarget.MasterClient, state, index);
        }
    }

    [PunRPC]
    private void RPC_PlayerReady(bool state, int index)
    {
        playersReady[index] = state;

        for (int i = 0; i < playersReady.Length; i++)
        {
            if (!playersReady[i])
            {
                return;
            }
        }

        // load the game scene
    }
}

public class Hello
{
    public Hello()
    {

    }
}