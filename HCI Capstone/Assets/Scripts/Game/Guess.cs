using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using System;
using UnityEngine.UI;
using Mapbox.Unity.Map;
using Unity.VisualScripting;
using static System.Net.Mime.MediaTypeNames;

public class Guess : MonoBehaviour
{
    private PhotonView photonView; // needed to do RPCs

    [SerializeField] private ChooseWord chooseWord;
    [SerializeField] private GPSControl gpsControl;
    [SerializeField] private GPSDraw gpsDraw;
    [SerializeField] private Draw draw;
    [SerializeField] private AbstractMap map;
    [SerializeField] private Scoreboard scoreboard;

    private ActorList<bool> playersReady; // a list of actors and boolean pairs to mark who is ready
    private ActorList<bool> playersFinished; // a list of actors and boolean pairs to mark who is finished
    private bool isPlayersFinished; // need this flag because host will run function twice due to desync in RPC

    private Coroutine currentTimer; // current timer coroutine

    private int PreTimerPeriod = 3; // number of seconds for the pre drawing timer
    private int timerPeriod; // number of seconds for the drawing timer
    private int TransitionTimerPeriod = 5;

    // pre timer window UI
    [SerializeField] private GameObject preGuessUI;
    [SerializeField] private TMPro.TMP_Text progressText;

    [SerializeField] private GameObject guessUI;
    [SerializeField] private TMPro.TMP_Text timerText; // timer text field
    private int timeLeft;

    //[SerializeField] private GameObject wordDisplay;
    [SerializeField] private GameObject showWordButton;
    [SerializeField] private GameObject hideWordButton;

    [SerializeField] private GameObject chatUI;
    [SerializeField] private ChatManager chat;
    [SerializeField] private TMPro.TMP_InputField inputField;

    [SerializeField] private GameObject waitingUI; // UI for waiting for all players to finsh drawing
    [SerializeField] private GameObject transitionUI;

    public Action OnFinished;

    private int roundNum;
    private int roundActorNumber;

    private int numHints;
    private int currHint;

    private bool wordGuessed;
    private bool hasGuessed;

    [SerializeField] private TMPro.TMP_Text hintText;
    [SerializeField] private TMPro.TMP_Text underScoreText;

    // Image Stuff
    [SerializeField] private RawImage guessedDrawing;
    [SerializeField] private int chunkSize = 1024;
    [SerializeField] private float timeBetweenChunks = 0.003f;

    private Texture2D bufferedTexture;
    private int textureChunks;
    private byte[] textureData;

    private double drawingLatitude;
    private double drawingLongitude;


    // is Sending? maybe for reconnecting when there is a lost connection?

    private void OnEnable()
    {
        inputField.onTouchScreenKeyboardStatusChanged.AddListener(SendWord);
    }

    private void OnDisable()
    {
        inputField.onTouchScreenKeyboardStatusChanged.RemoveListener(SendWord);
    }

    public void SetUp(RoomOptionsData roomSettings)
    {
        photonView = GetComponent<PhotonView>();
        timerPeriod = roomSettings.GuessWordTime;
        numHints = roomSettings.NumHints;
        roundNum = 0;
    }

    /// <summary>
    /// Begins the drawing phase of the game.
    /// </summary>
    public void Initialize()
    {
        roundNum++;
        timerText.text = timerPeriod.ToString();
        preGuessUI.SetActive(true);
        progressText.text = "";

        // do this in check turn
        //wordDisplay.transform.GetChild(0).GetComponent<TMPro.TMP_Text>().text = chooseWord.ChosenWords[PhotonNetwork.LocalPlayer.ActorNumber];

        showWordButton.SetActive(false);
        hideWordButton.SetActive(false);
        //wordDisplay.SetActive(false);

        hintText.gameObject.SetActive(true);
        hintText.text = "";
        underScoreText.gameObject.SetActive(true);
        underScoreText.text = "";
        currHint = 1;

        wordGuessed = false;
        hasGuessed = false;

        chatUI.SetActive(true);
        inputField.interactable = true;

        if (PhotonNetwork.IsMasterClient)
        {
            if (playersReady == null) playersReady = new ActorList<bool>(PhotonNetwork.PlayerList);
            isPlayersFinished = false;
        }

        CheckTurn();
    }

    [PunRPC]
    private void RPC_InitializeGuess(int actorNumber)
    {
        if (playersReady == null) playersReady = new ActorList<bool>(PhotonNetwork.PlayerList);
        playersReady[actorNumber] = true;

        // check if all the players are ready
        foreach (ActorList<bool>.ActorData<bool> actorElem in playersReady)
        {
            if (!actorElem.data) return;
        }

        photonView.RPC("RPC_InitializeContinueGuess", RpcTarget.Others);
        SendBlankWord();
        currentTimer = StartCoroutine(PreTimer());

        if (playersFinished == null) playersFinished = new ActorList<bool>(PhotonNetwork.PlayerList);
        playersFinished[roundActorNumber] = true;
    }

    [PunRPC]
    private void RPC_InitializeContinueGuess()
    {
        currentTimer = StartCoroutine(PreTimer());
    }

    /// <summary>
    /// Checks if it is the local client's turn to send their drawing to the other clients.
    /// </summary>
    /// <returns>True if it is local player's turn.</returns>
    private bool CheckTurn()
    {
        int numActorNumbersLower = 0;
        for (int i = 0; i < PhotonNetwork.PlayerListOthers.Length; i++) // getting playerlist is faster despite doing an unnessary loop
        {
            if (PhotonNetwork.PlayerListOthers[i].ActorNumber < PhotonNetwork.LocalPlayer.ActorNumber)
            {
                numActorNumbersLower++;
            }
        }

        if (numActorNumbersLower == roundNum - 1) // TODO change this depending if round num starts at 1 or 0
        {
            roundActorNumber = PhotonNetwork.LocalPlayer.ActorNumber;

            int fontSize = GetAutoFontSize(chooseWord.ChosenWords[PhotonNetwork.LocalPlayer.ActorNumber]);
            hintText.fontSize = fontSize;
            hintText.text = $"<mspace=mspace={fontSize}>{chooseWord.ChosenWords[PhotonNetwork.LocalPlayer.ActorNumber]}</mspace>";
            // the gameobject should be disabled
            hintText.gameObject.SetActive(false);

            StartCoroutine(SendImageBuffered(RenderTextureToTexture2D(draw.DrawingRenderTexture), chunkSize, timeBetweenChunks));
            return true;
        }
        else
        {
            return false;
        }
    }

    /// <summary>
    /// Converts the current image on a RenderTexture to a Texture2D.
    /// Original Source: https://stackoverflow.com/questions/44264468/convert-rendertexture-to-texture2d
    /// </summary>
    /// <param name="renderTexture">The RenderTexture you would like to copy.</param>
    /// <returns>The image converted to Texture2D.</returns>
    private Texture2D RenderTextureToTexture2D(RenderTexture renderTexture)
    {
        Texture2D texture2D = new Texture2D(renderTexture.width, renderTexture.height, TextureFormat.ARGB32, false);
        // ReadPixels looks at the active RenderTexture.
        RenderTexture.active = renderTexture;
        texture2D.ReadPixels(new Rect(0, 0, renderTexture.width, renderTexture.height), 0, 0);
        texture2D.Apply();
        return texture2D;
    }

    /// <summary>
    /// Sends an image to all other clients in the room in byte chunks specified by a number of segments. 
    /// Chunks are sent with a specified time between remote procedure calls.
    /// </summary>
    /// <param name="image">The image you would like to send.</param>
    /// <param name="chunkSize">The size of int chunks you would like to split the image into.</param>
    /// <param name="timeBetweenRPCs">The time between a remote procedure call that sends a segment.</param>
    /// <returns>Coroutine method.</returns>
    private IEnumerator SendImageBuffered(Texture2D image, int chunkSize, float timeBetweenRPCs)
    {
        int[] data = OptimizeDrawing(image);

        float fraction = data.Length / (float)chunkSize;
        int chunks = Mathf.CeilToInt(fraction);
        progressText.text = $"{0}/{chunks}";

        photonView.RPC("RPC_StartReceiveImage", RpcTarget.Others,
            PhotonNetwork.LocalPlayer.ActorNumber, gpsDraw.CanvasLatitude, gpsDraw.CanvasLongitude, 
            image.width, image.height, chunks); // tell other clients to start receiving image
        Debug.Log("Local player started sending image to other players.");

        yield return new WaitForSeconds(timeBetweenRPCs);

        int lengthRemaining = data.Length;
        for (int i = 0; i < chunks; i++)
        {
            int length = Mathf.Min(data.Length / chunks, lengthRemaining); // if at the tail end of the array use the value that is remaining
            lengthRemaining -= data.Length / chunks; // subtracting a segment each time;

            int[] imageChunk = new int[length];

            for (int a = 0; a < length; a++)
            {
                imageChunk[a] = data[((data.Length / chunks) * i) + a];
            }

            photonView.RPC("RPC_ReceiveImageSegment", RpcTarget.Others, imageChunk, i);
            Debug.Log("Local player sent another image segment to other players.");
            progressText.text = $"{i + 1}/{chunks}";

            yield return new WaitForSeconds(timeBetweenRPCs);
        }

        gpsControl.ResetCameraOnLocation(gpsDraw.CanvasLatitude, gpsDraw.CanvasLongitude);

        if (PhotonNetwork.IsMasterClient)
        {
            playersReady[PhotonNetwork.LocalPlayer.ActorNumber] = true;

            if (playersFinished == null) playersFinished = new ActorList<bool>(PhotonNetwork.PlayerList);
            playersFinished[roundActorNumber] = true; 

            // check if all the players are ready
            bool ready = true;
            foreach (ActorList<bool>.ActorData<bool> actorElem in playersReady)
            {
                if (!actorElem.data) ready = false;
            }

            if (ready)
            {
                photonView.RPC("RPC_InitializeContinueGuess", RpcTarget.Others);
                SendBlankWord();
                currentTimer = StartCoroutine(PreTimer());
            }
        }
        else
        {
            photonView.RPC("RPC_InitializeGuess", RpcTarget.MasterClient, PhotonNetwork.LocalPlayer.ActorNumber);
        }
    }

    /// <summary>
    /// Optimizes the amount of data contained in a drawn image by returning an int 
    /// array containing only data for colored pixels. This reduces the amount of data 
    /// to be sent over the internet.
    /// </summary>
    /// <param name="image">The image you would like to optimize.</param>
    /// <returns>An int array where each array element represents a position and a color.</returns>
    private int[] OptimizeDrawing(Texture2D image)
    {
        int count = 0;
        byte[] rawData = image.GetRawTextureData();
        int[] unreducedData = new int[rawData.Length / 4];

        for (int i = 0; i < unreducedData.Length; i++)
        {
            if (rawData[i * 4] != 0) // ignore empty space
            {
                //int a = rawData[i * 4];
                //int r = rawData[(i * 4) + 1];
                //int g = rawData[(i * 4) + 2];
                //int b = rawData[(i * 4) + 3];
                if (rawData[(i * 4) + 1] == 255) // red
                {
                    unreducedData[count] = (i * 4) << 4;
                }
                else if (rawData[(i * 4) + 2] == 255) // green
                {
                    unreducedData[count] = ((i * 4) << 4) + 1;
                }
                else if (rawData[(i * 4) + 3] == 255) // blue
                {
                    unreducedData[count] = ((i * 4) << 4) + 2;
                }
                else if (rawData[(i * 4) + 1] > 210 && rawData[(i * 4) + 1] < 230) // yellow
                {
                    unreducedData[count] = ((i * 4) << 4) + 3;
                }
                else if (rawData[(i * 4) + 1] > 0 && rawData[(i * 4) + 1] < 20) // black
                {
                    unreducedData[count] = ((i * 4) << 4) + 4;
                }
                else if (rawData[(i * 4) + 2] > 230 && rawData[(i * 4) + 2] < 250) // white
                {
                    unreducedData[count] = ((i * 4) << 4) + 5;
                }

                count++;
            }
        }

        int[] data = new int[count];

        for (int i = 0; i < count; i++)
        {
            data[i] = unreducedData[i];
        }
        return data;
    }

    /// <summary>
    /// Function run by receiving client containing necessary information to begin receiving an image.
    /// </summary>
    /// <param name="actorNumber">The actorNumber of the sending client.</param>
    /// <param name="latitude">Latitude of the image on the map.</param>
    /// <param name="longitude">Longitude of the image on the map.</param>
    /// <param name="width">Width of the image.</param>
    /// <param name="height">Height of the image.</param>
    /// <param name="chunks">Number of chunks the image is split into.</param>
    [PunRPC]
    private void RPC_StartReceiveImage(int actorNumber, double latitude, double longitude,
        int width, int height, int chunks)
    {
        roundActorNumber = actorNumber;
        drawingLatitude = latitude;
        drawingLongitude = longitude;
        bufferedTexture = new Texture2D(width, height, TextureFormat.ARGB32, false);
        textureChunks = chunks;
        textureData = new byte[width * height * 4];
        Debug.Log("Local player received start of an image from another player.");
        progressText.text = $"{0}/{chunks}";

        if (chunks == 0)
        {
            if (PhotonNetwork.IsMasterClient)
            {
                playersReady[PhotonNetwork.LocalPlayer.ActorNumber] = true;

                if (playersFinished == null) playersFinished = new ActorList<bool>(PhotonNetwork.PlayerList);
                playersFinished[roundActorNumber] = true;
                //StopGuessing();

                // check if all the players are ready
                foreach (ActorList<bool>.ActorData<bool> actorElem in playersReady)
                {
                    if (!actorElem.data) return;
                }

                photonView.RPC("RPC_InitializeContinueGuess", RpcTarget.Others);
                SendBlankWord();
                currentTimer = StartCoroutine(PreTimer());
            }
            else
            {
                photonView.RPC("RPC_InitializeGuess", RpcTarget.MasterClient, PhotonNetwork.LocalPlayer.ActorNumber);
            }
        }
    }

    /// <summary>
    /// Function run by receiving client when a chunk of ints for an image is received.
    /// </summary>
    /// <param name="imageChunk">A chunk of ints that contain the position of the color in an image.</param>
    /// <param name="chunkIndex">The index of the chunk of bytes.</param>
    [PunRPC]
    private void RPC_ReceiveImageSegment(int[] imageChunk, int chunkIndex) // not sure if this runs if imageChunk is length 0
    {
        // Reconstruct the drawing from the chunk
        for (int i = 0; i < imageChunk.Length; i++)
        {
            switch (imageChunk[i] & 0x0F)
            {
                case 0: // red
                    textureData[imageChunk[i] >> 4] = 255;
                    textureData[(imageChunk[i] >> 4) + 1] = 255;
                    break;
                case 1: // green
                    textureData[imageChunk[i] >> 4] = 255;
                    textureData[(imageChunk[i] >> 4) + 2] = 255;
                    break;
                case 2: // blue
                    textureData[imageChunk[i] >> 4] = 255;
                    textureData[(imageChunk[i] >> 4) + 2] = 168;
                    textureData[(imageChunk[i] >> 4) + 3] = 255;
                    break;
                case 3: // yellow
                    textureData[imageChunk[i] >> 4] = 255;
                    textureData[(imageChunk[i] >> 4) + 1] = 235;
                    textureData[(imageChunk[i] >> 4) + 2] = 235;
                    break;
                case 4: // black
                    textureData[imageChunk[i] >> 4] = 255;
                    textureData[(imageChunk[i] >> 4) + 1] = 5;
                    textureData[(imageChunk[i] >> 4) + 2] = 5;
                    textureData[(imageChunk[i] >> 4) + 3] = 5;
                    break;
                case 5: // white
                    textureData[imageChunk[i] >> 4] = 255;
                    textureData[(imageChunk[i] >> 4) + 1] = 245;
                    textureData[(imageChunk[i] >> 4) + 2] = 245;
                    textureData[(imageChunk[i] >> 4) + 3] = 245;
                    break;
            }
        }
        Debug.Log("Local player received part of an image from another player.");
        progressText.text = $"{chunkIndex + 1}/{textureChunks}";

        if (chunkIndex == textureChunks - 1)
        {
            // finished getting all of the chunks
            bufferedTexture.LoadRawTextureData(textureData);
            bufferedTexture.Apply();
            textureData = null;
            guessedDrawing.texture = bufferedTexture;

            // move image to where the image was drawn
            gpsControl.ResetCameraOnLocation(drawingLatitude, drawingLongitude);

            if (PhotonNetwork.IsMasterClient)
            {
                playersReady[PhotonNetwork.LocalPlayer.ActorNumber] = true;

                if (playersFinished == null) playersFinished = new ActorList<bool>(PhotonNetwork.PlayerList);
                playersFinished[roundActorNumber] = true;

                // check if all the players are ready
                foreach (ActorList<bool>.ActorData<bool> actorElem in playersReady)
                {
                    if (!actorElem.data) return;
                }

                photonView.RPC("RPC_InitializeContinueGuess", RpcTarget.Others);
                SendBlankWord();
                currentTimer = StartCoroutine(PreTimer());
            }
            else
            {
                photonView.RPC("RPC_InitializeGuess", RpcTarget.MasterClient, PhotonNetwork.LocalPlayer.ActorNumber);
            }
        }
    }

    /// <summary>
    /// The timer before the drawing phase starts.
    /// </summary>
    /// <returns>Coroutine waits 1 second intervals.</returns>
    private IEnumerator PreTimer()
    {
        for (int i = PreTimerPeriod; i > 0; i--)
        {
            yield return new WaitForSeconds(1f);
        }
        StartGuessing();
    }

    private void StartGuessing()
    {
        preGuessUI.SetActive(false);
        guessUI.SetActive(true);

        // show the image
        if (roundActorNumber == PhotonNetwork.LocalPlayer.ActorNumber)
        {
            // display the render texture
            gpsDraw.ShowGPSCanvas();

            // disable input field
            // enable display word button

            showWordButton.SetActive(true);
            hideWordButton.SetActive(false);

            inputField.interactable = false;
        }
        else
        {
            // display the image
            guessedDrawing.transform.parent.parent.gameObject.SetActive(true);

            //currentTimer = StartCoroutine(Timer());
        }
        currentTimer = StartCoroutine(Timer());
    }

    private IEnumerator Timer()
    {
        Debug.Log("Started guessing timer.");
        for (timeLeft = timerPeriod; timeLeft > 0; timeLeft--)
        {
            if (PhotonNetwork.IsMasterClient) ShowHint(timeLeft);
            timerText.text = timeLeft.ToString();
            yield return new WaitForSeconds(1f);
        }
        StopGuessing();
    }

    // Sending a float to other clients could lead to different results, so cast to an int.
    private int GetAutoFontSize(string word)
    {
        return Mathf.Min((int)(hintText.rectTransform.rect.width / word.Length), 150);
    }

    // TODO create a list of each letter so the same letters are not picked randomly
    private void SendBlankWord()
    {
        // use the word and convert it to blanks
        string word = chooseWord.ChosenWords[roundActorNumber];
        string blankWord = "";

        for (int i = 0; i < word.Length; i++)
        {
            if (word[i] == ' ')
            {
                blankWord += ' ';
            }
            else
            {
                blankWord += '_';
            }
        }

        int fontSize = GetAutoFontSize(blankWord);
        photonView.RPC("RPC_ReceiveBlankWord", RpcTarget.Others, blankWord, fontSize);

        if (PhotonNetwork.LocalPlayer.ActorNumber != roundActorNumber)
        {
            hintText.fontSize = fontSize;
            // hintText needs to be filled with spaces
            string spaces = "";
            for (int i = 0; i < blankWord.Length; i++)
            {
                if (blankWord[i] == ' ')
                {
                    spaces += " ";
                }
                else
                {
                    spaces += "_";
                }
            }
            hintText.text = $"<mspace=mspace={fontSize}>{spaces}</mspace>";
        }
        underScoreText.fontSize = fontSize;
        underScoreText.text = $"<mspace=mspace={fontSize}>{blankWord}</mspace>";
    }

    [PunRPC]
    private void RPC_ReceiveBlankWord(string blankWord, int fontSize)
    {
        if (PhotonNetwork.LocalPlayer.ActorNumber != roundActorNumber)
        {
            hintText.fontSize = fontSize;
            // hintText needs to be filled with spaces
            string spaces = "";
            for (int i = 0; i < blankWord.Length; i++)
            {
                if (blankWord[i] == ' ')
                {
                    spaces += " ";
                }
                else
                {
                    spaces += "_";
                }
            }
            hintText.text = $"<mspace=mspace={fontSize}>{spaces}</mspace>";
        }
        underScoreText.fontSize = fontSize;
        underScoreText.text = $"<mspace=mspace={fontSize}>{blankWord}</mspace>";
    }

    // TODO decrease points based on currHint
    private void ShowHint(int timeLeft)
    {
        string word = chooseWord.ChosenWords[roundActorNumber];

        if (timeLeft == timerPeriod - ((timerPeriod / 2) + (((timerPeriod / 2) / numHints) * (currHint - 1))))
        {
            int hintIndex = UnityEngine.Random.Range(0, word.Length);
            if (word[hintIndex] == ' ')
            {
                hintIndex++;
            }

            int fullHintIndex = underScoreText.text.IndexOf('>') + 1 + hintIndex;
            // send the rpc with the character and the hint index
            photonView.RPC("RPC_ShowHint", RpcTarget.Others, word[hintIndex].ToString(), fullHintIndex);

            if (PhotonNetwork.LocalPlayer.ActorNumber != roundActorNumber)
            {
                hintText.text = hintText.text.Substring(0, fullHintIndex) + word[hintIndex] + hintText.text.Substring(fullHintIndex + 1, hintText.text.Length - (fullHintIndex + 1));
            }

            if (currHint < numHints)
            {
                currHint++;
            }
        }
    }

    [PunRPC]
    private void RPC_ShowHint(string letter, int index)
    {
        if (PhotonNetwork.LocalPlayer.ActorNumber != roundActorNumber)
        {
            hintText.text = hintText.text.Substring(0, index) + letter + hintText.text.Substring(index + 1, hintText.text.Length - (index + 1));
        }
    }

    public void ShowWord()
    {
        showWordButton.SetActive(false);
        hideWordButton.SetActive(true);
        
        //wordDisplay.SetActive(true);
        hintText.gameObject.SetActive(true);
    }
    public void HideWord()
    {
        showWordButton.SetActive(true);
        hideWordButton.SetActive(false);

        //wordDisplay.SetActive(false);
        hintText.gameObject.SetActive(false);
    }

    /// <summary>
    /// Can either be triggered by the timer running out or the everyone guessing the word/.
    /// </summary>
    public void StopGuessing()
    {
        if (currentTimer != null)
        {
            StopCoroutine(currentTimer); // TODO may need to check null?
        }

        // TODO show waiting screen in the meantime
        guessUI.SetActive(false);
        waitingUI.SetActive(true);

        inputField.interactable = false;

        if (PhotonNetwork.IsMasterClient)
        {
            if (!isPlayersFinished)
            {
                // set your own actornumber to finished
                playersFinished[PhotonNetwork.LocalPlayer.ActorNumber] = true; // TODO getting a nullref here

                foreach (ActorList<bool>.ActorData<bool> actorElem in playersFinished)
                {
                    if (!actorElem.data) return;
                }

                // tell all other clients that this game state is over
                photonView.RPC("RPC_FinishGuessing", RpcTarget.Others, chooseWord.ChosenWords[roundActorNumber]);
                isPlayersFinished = true;

                waitingUI.SetActive(false);
                currentTimer = StartCoroutine(TransitionTimer(chooseWord.ChosenWords[roundActorNumber]));
            }
        }
        else
        {
            // tell the master client to set your actornumber to finished
            photonView.RPC("RPC_StopGuessing", RpcTarget.MasterClient, PhotonNetwork.LocalPlayer.ActorNumber);
        }
    }

    /// <summary>
    /// Tells the master client that another client has finished guessing.
    /// This function will check if all players are finished.
    /// Run by the master client.
    /// </summary>
    /// <param name="actorNumber">The photon actor number of the client that is finished.</param>
    [PunRPC]
    private void RPC_StopGuessing(int actorNumber)
    {
        if (!isPlayersFinished)
        {
            // check the finished states of all the users
            playersFinished[actorNumber] = true;

            foreach (ActorList<bool>.ActorData<bool> actorElem in playersFinished)
            {
                if (!actorElem.data) return;
            }

            // tell all other clients that this game state is over
            photonView.RPC("RPC_FinishGuessing", RpcTarget.Others, chooseWord.ChosenWords[roundActorNumber]);
            isPlayersFinished = true;

            if (PhotonNetwork.LocalPlayer.ActorNumber == roundActorNumber)
            {
                guessUI.SetActive(false);
                if (currentTimer != null)
                {
                    // Debug.Log("Stopping coroutine.");
                    StopCoroutine(currentTimer); // TODO may need to check null?
                }
            }

            waitingUI.SetActive(false);
            // need a transition here

            // show what the word was
            currentTimer = StartCoroutine(TransitionTimer(chooseWord.ChosenWords[roundActorNumber]));
        }
    }

    /// <summary>
    /// Run by the non Master clients when done guessing for one of the images.
    /// </summary>
    [PunRPC]
    private void RPC_FinishGuessing(string lastWord)
    {
        if (currentTimer != null)
        {
            // Debug.Log("Stopping coroutine.");
            StopCoroutine(currentTimer); // TODO may need to check null?
        }

        guessUI.SetActive(false);
        waitingUI.SetActive(false);

        // the word was

        // need a transition here 
        currentTimer = StartCoroutine(TransitionTimer(lastWord));
    }

    /// <summary>
    /// The timer transitioning from guessing one picture to the next.
    /// </summary>
    /// <returns>Coroutine waits 1 second intervals.</returns>
    private IEnumerator TransitionTimer(string lastWord)
    {
        if (!hasGuessed)
        {
            // fill the blank word 
            chat.SendMessageToChat($"The word from the picture was {lastWord}.", Message.MessageType.info); // only need to show this if the user hasn't guessed the word
        }

        // display the word in the blank
        if (PhotonNetwork.LocalPlayer.ActorNumber != roundActorNumber)
        {
            hintText.text = hintText.text.Substring(0, underScoreText.text.IndexOf('>') + 1) + 
                lastWord + hintText.text.Substring(underScoreText.text.LastIndexOf('<'));
        }

        transitionUI.SetActive(true);
        for (int i = TransitionTimerPeriod; i > 0; i--)
        {
            yield return new WaitForSeconds(1f);
        }

        showWordButton.SetActive(false);
        hideWordButton.SetActive(false);
        //wordDisplay.SetActive(false);
        // hintText will be set to inactive on Initialize

        gpsDraw.HideGPSCanvas();
        guessedDrawing.transform.parent.parent.gameObject.SetActive(false);

        transitionUI.SetActive(false);
        chat.Clear();

        hintText.gameObject.SetActive(false);
        underScoreText.gameObject.SetActive(false);

        playersReady = null;
        playersFinished = null;

        if (roundNum == PhotonNetwork.PlayerList.Length)
        {
            roundNum = 0;
            guessedDrawing.texture = null;
            chatUI.SetActive(false);
            gpsControl.ResetCamera();
            OnFinished();
        }
        else
        {
            Initialize();
        }
    }

    // run this when enter is pressed and there are contents in the input field
    public void SendWord(TouchScreenKeyboard.Status status)
    {
        if (status == TouchScreenKeyboard.Status.Done)
        {
            Debug.Log("Sending message.");
            string word = inputField.text; // contents of the input field
            inputField.text = "";

            if (PhotonNetwork.IsMasterClient)
            {
                if (word.ToLower().Equals(chooseWord.ChosenWords[roundActorNumber].ToLower()))
                {
                    // player guessed correctly
                    // assign score
                    int pointsEarnedGuesser = 100 + (int)((float)timeLeft / timerPeriod * 50);

                    Scoreboard.Score scoreGuesser = scoreboard.Scores[PhotonNetwork.LocalPlayer.ActorNumber];
                    scoreGuesser.pointsThisRound += pointsEarnedGuesser;
                    scoreboard.Scores[PhotonNetwork.LocalPlayer.ActorNumber] = scoreGuesser;

                    int pointsEarnedGuessee;
                    if (!wordGuessed) // check if someone already guessed their word
                    {
                        pointsEarnedGuessee = 100;

                        Scoreboard.Score scoreGuessee = scoreboard.Scores[roundActorNumber];
                        scoreGuessee.pointsThisRound += pointsEarnedGuessee;
                        scoreboard.Scores[roundActorNumber] = scoreGuessee;

                        wordGuessed = true;
                    }
                    else
                    {
                        pointsEarnedGuessee = 0;
                    }

                    string guesseeName = "";
                    for (int i = 0; i < PhotonNetwork.PlayerListOthers.Length; i++)
                    {
                        if (PhotonNetwork.PlayerListOthers[i].ActorNumber == roundActorNumber)
                        {
                            guesseeName = PhotonNetwork.PlayerListOthers[i].NickName;
                        }
                    }

                    photonView.RPC("RPC_SomeoneGuessedCorrectly", RpcTarget.Others, PhotonNetwork.LocalPlayer.ActorNumber, pointsEarnedGuesser, pointsEarnedGuessee);

                    string chatMessage = $"You correctly guessed that {word} was the word! You earned {pointsEarnedGuesser} points. " +
                        $"{guesseeName} earned {pointsEarnedGuessee} points for someone guessing their word.";

                    // send message in chat
                    chat.SendMessageToChat(chatMessage, Message.MessageType.info);
                    hasGuessed = true;
                    StopGuessing();
                }
                else
                {
                    // player guessed incorrectly
                    photonView.RPC("RPC_GuessedIncorrectly", RpcTarget.Others, PhotonNetwork.LocalPlayer.ActorNumber, word);

                    string chatMessage = $"{PhotonNetwork.LocalPlayer.NickName}: {word}";
                    // send message in chat
                    chat.SendMessageToChat(chatMessage, Message.MessageType.playerMessage);
                }
            }
            else
            {
                photonView.RPC("RPC_SendToClient", RpcTarget.MasterClient, PhotonNetwork.LocalPlayer.ActorNumber, word);
            }
        }
    }
    
    // TODO make it so the client being guessed upon gets points as well
    // gets sent to the master client
    [PunRPC]
    private void RPC_SendToClient(int actorNumber, string word)
    {
        //If client guesses correctly, mark them down as finished
        if (word.ToLower().Equals(chooseWord.ChosenWords[roundActorNumber].ToLower()))
        {
            // player guessed correctly
            // assign score
            int pointsEarnedGuesser = 100 + (int)((float)timeLeft / timerPeriod * 50);

            Scoreboard.Score scoreGuesser = scoreboard.Scores[actorNumber];
            scoreGuesser.pointsThisRound += pointsEarnedGuesser;
            scoreboard.Scores[actorNumber] = scoreGuesser;

            int pointsEarnedGuessee;
            if (!wordGuessed) // check if someone already guessed their word
            {
                pointsEarnedGuessee = 100;

                Scoreboard.Score scoreGuessee = scoreboard.Scores[roundActorNumber];
                scoreGuessee.pointsThisRound += pointsEarnedGuessee;
                scoreboard.Scores[roundActorNumber] = scoreGuessee;

                wordGuessed = true;
            }
            else
            {
                pointsEarnedGuessee = 0;
            }

            string guesserName = "";
            string guesseeName = "";
            for (int i = 0; i < PhotonNetwork.PlayerListOthers.Length; i++)
            {
                if (PhotonNetwork.PlayerListOthers[i].ActorNumber == actorNumber)
                {
                    guesserName = PhotonNetwork.PlayerListOthers[i].NickName;
                    photonView.RPC("RPC_YouGuessedCorrectly", PhotonNetwork.PlayerListOthers[i], actorNumber, word, pointsEarnedGuesser, pointsEarnedGuessee);
                }
                else if (PhotonNetwork.PlayerListOthers[i].ActorNumber == roundActorNumber)
                {
                    guesseeName = PhotonNetwork.PlayerListOthers[i].NickName;
                }
            }

            photonView.RPC("RPC_SomeoneGuessedCorrectly", RpcTarget.Others, actorNumber, pointsEarnedGuesser, pointsEarnedGuessee);

            string chatMessage;
            if (PhotonNetwork.LocalPlayer.ActorNumber == roundActorNumber)
            {
                chatMessage = $"{guesserName} has correctly guessed the word! They earned {pointsEarnedGuesser} points. " +
                    $"You earned {pointsEarnedGuessee} points for someone guessing your word.";
            }
            else
            {
                chatMessage = $"{guesserName} has correctly guessed the word! They earned {pointsEarnedGuesser} points. " +
                    $"{guesseeName} earned {pointsEarnedGuessee} points for someone guessing their word.";
            }

            // send message in chat
            chat.SendMessageToChat(chatMessage, Message.MessageType.info);
        }
        else
        {
            // player guessed incorrectly

            photonView.RPC("RPC_GuessedIncorrectly", RpcTarget.Others, actorNumber, word);

            string name = "";
            for (int i = 0; i < PhotonNetwork.PlayerList.Length; i++)
            {
                if (PhotonNetwork.PlayerListOthers[i].ActorNumber == actorNumber && PhotonNetwork.LocalPlayer.ActorNumber != actorNumber)
                {
                    name = PhotonNetwork.PlayerListOthers[i].NickName;
                    break;
                }
            }

            string chatMessage = $"{name}: {word}";
            // send message in chat
            chat.SendMessageToChat(chatMessage, Message.MessageType.playerMessage);
        }
    }

    [PunRPC]
    private void RPC_YouGuessedCorrectly(int actorNumber, string word, int pointsEarnedGuesser, int pointsEarnedGuessee)
    {
        Scoreboard.Score scoreGuesser = scoreboard.Scores[actorNumber];
        scoreGuesser.pointsThisRound += pointsEarnedGuesser;
        scoreboard.Scores[actorNumber] = scoreGuesser;

        // check if someone already guessed their word
        Scoreboard.Score scoreGuessee = scoreboard.Scores[roundActorNumber];
        scoreGuessee.pointsThisRound += pointsEarnedGuessee;
        scoreboard.Scores[roundActorNumber] = scoreGuessee;

        string guesseeName = "";
        for (int i = 0; i < PhotonNetwork.PlayerListOthers.Length; i++)
        {
            if (PhotonNetwork.PlayerListOthers[i].ActorNumber == roundActorNumber)
            {
                guesseeName = PhotonNetwork.PlayerListOthers[i].NickName;
            }
        }

        string chatMessage = $"You correctly guessed that {word} was the word! You earned {pointsEarnedGuesser} points. " +
            $"{guesseeName} earned {pointsEarnedGuessee} points for someone guessing their word.";

        // send message in chat
        chat.SendMessageToChat(chatMessage, Message.MessageType.info);
        hasGuessed = true;
        StopGuessing();
    }

    [PunRPC]
    private void RPC_SomeoneGuessedCorrectly(int actorNumber, int pointsEarnedGuesser, int pointsEarnedGuessee)
    {
        if (PhotonNetwork.LocalPlayer.ActorNumber != actorNumber)
        {
            //if actorNumber == LocalPlayer.ActorNumber, then turn off InputField box
            Scoreboard.Score scoreGuesser = scoreboard.Scores[actorNumber];
            scoreGuesser.pointsThisRound += pointsEarnedGuesser;
            scoreboard.Scores[actorNumber] = scoreGuesser;

            // check if someone already guessed their word
            Scoreboard.Score scoreGuessee = scoreboard.Scores[roundActorNumber];
            scoreGuessee.pointsThisRound += pointsEarnedGuessee;
            scoreboard.Scores[roundActorNumber] = scoreGuessee;

            string guesserName = "";
            string guesseeName = "";
            for (int i = 0; i < PhotonNetwork.PlayerListOthers.Length; i++)
            {
                if (PhotonNetwork.PlayerListOthers[i].ActorNumber == actorNumber)
                {
                    guesserName = PhotonNetwork.PlayerListOthers[i].NickName;
                }
                else if (PhotonNetwork.PlayerListOthers[i].ActorNumber == roundActorNumber)
                {
                    guesseeName = PhotonNetwork.PlayerListOthers[i].NickName;
                }
            }

            string chatMessage;
            if (PhotonNetwork.LocalPlayer.ActorNumber == roundActorNumber)
            {
                chatMessage = $"{guesserName} has correctly guessed the word! They earned {pointsEarnedGuesser} points. " +
                    $"You earned {pointsEarnedGuessee} points for someone guessing your word.";
            }
            else
            {
                chatMessage = $"{guesserName} has correctly guessed the word! They earned {pointsEarnedGuesser} points. " +
                    $"{guesseeName} earned {pointsEarnedGuessee} points for someone guessing their word.";
            }

            // send message in chat
            chat.SendMessageToChat(chatMessage, Message.MessageType.info);
        }
    }

    [PunRPC]
    private void RPC_GuessedIncorrectly(int actorNumber, string word)
    {
        string name = "";
        for (int i = 0; i < PhotonNetwork.PlayerList.Length; i++)
        {
            if (PhotonNetwork.PlayerList[i].ActorNumber == actorNumber)
            {
                name = PhotonNetwork.PlayerList[i].NickName;
                break;
            }
        }

        // send to the chat
        string chatMessage = $"{name}: {word}";
        // send message in chat
        chat.SendMessageToChat(chatMessage, Message.MessageType.playerMessage);
    }

    // showerd what the word was twice
}
