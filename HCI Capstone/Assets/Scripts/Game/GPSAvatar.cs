using Mapbox.Unity.Location;
using UnityEngine;

public class GPSAvatar : MonoBehaviour
{
    /// <summary>
    /// The target the avatar will follow.
    /// The avatar will smoothly interpolate towards the targets position.
    /// </summary>
    [SerializeField] private Transform target;
    [SerializeField] private LocationProviderFactory locationProviderFactory;

    private readonly float InterpolationRate = 0.5f;
    private readonly float SmoothTime = 4f;

    private void OnEnable()
    {
        // teleport to the default location
        transform.position = target.position;
    }

    // Update is called once per frame
    private void Update()
    {
        float currentVelocity = 0;
        transform.rotation = Quaternion.Euler(0, Mathf.SmoothDampAngle(transform.rotation.eulerAngles.y,
            locationProviderFactory.DefaultLocationProvider.CurrentLocation.DeviceOrientation, 
            ref currentVelocity, SmoothTime * Time.deltaTime), 0);

        transform.position = Vector3.Lerp(transform.position, target.position, 
            InterpolationRate * Time.deltaTime);
    }
}
