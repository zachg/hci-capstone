using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Source: https://youtu.be/IRAeJgGkjHk
/// </summary>
public class ChatManager : MonoBehaviour
{
    [SerializeField] private int maxMessages = 25;

    [SerializeField] private Color playerMessage;
    [SerializeField] private Color info;

    [SerializeField] private GameObject chatPanel;
    [SerializeField] private GameObject textObject;

    [SerializeField] private List<Message> messageList = new List<Message>();

    public void SendMessageToChat(string text, Message.MessageType type)
    {
        if (messageList.Count > maxMessages)
        {
            Destroy(messageList[0].TextObject.gameObject);
            messageList.Remove(messageList[0]);
        }

        Message newMessage = new Message();

        newMessage.Text = text;

        GameObject newText = Instantiate(textObject, chatPanel.transform);

        newMessage.TextObject = newText.GetComponent<TMPro.TMP_Text>();

        newMessage.TextObject.text = newMessage.Text;
        newMessage.Type = type;
        newMessage.TextObject.color = MessageTypeColor(type);

        RectTransform rectTransform = newText.GetComponent<RectTransform>();
        rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, newMessage.TextObject.preferredHeight);

        messageList.Add(newMessage);
    }

    private Color MessageTypeColor(Message.MessageType type)
    {
        switch (type)
        {
            case Message.MessageType.playerMessage:
                return playerMessage;
            default:
                return info;
        }
    }

    public void Clear()
    {
        for (int i = messageList.Count; i > 0; i--)
        {
            Destroy(messageList[0].TextObject.gameObject);
            messageList.Remove(messageList[0]);
        }
    }
}

[System.Serializable]
public class Message
{
    private string text;
    private TMPro.TMP_Text textObject;
    private MessageType type;

    public enum MessageType
    {
        playerMessage,
        info
    }

    public string Text { get => text; set => text = value; }
    public TMPro.TMP_Text TextObject { get => textObject; set => textObject = value; }  
    public MessageType Type { get => type; set => type = value; }
}
