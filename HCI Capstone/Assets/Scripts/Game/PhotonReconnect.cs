using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using UnityEngine;

public class PhotonReconnect : MonoBehaviourPunCallbacks
{
    [SerializeField] private GameObject disconnectedMessage;

    private bool isMasterClient;

    private void Start()
    {
        isMasterClient = PhotonNetwork.IsMasterClient;
    }

    //public override void OnConnected()
    //{
    //    //Debug.Log("Reconnected to photon room.");
    //    Debug.Log("OnConnected");
    //    disconnectedMessage.SetActive(false);
    //}

    /// <summary>
    /// If the WiFi connection drops or the phone goes to sleep will attempt to reconnect.
    /// </summary>
    /// <param name="cause"></param>
    public override void OnDisconnected(DisconnectCause cause)
    {
        Debug.LogError("Disconnected from photon. Will now attempt reconnect.");
        disconnectedMessage?.SetActive(true);
        StartCoroutine(AttemptReconnect());
    }

    private IEnumerator AttemptReconnect()
    {
        yield return new WaitForSeconds(1);
        PhotonNetwork.ReconnectAndRejoin();

    }

    public override void OnJoinedRoom()
    {
        Debug.Log("Reconnected to photon room.");
        //Debug.Log("OnJoinedRoom");
        disconnectedMessage.SetActive(false);
        if (isMasterClient) PhotonNetwork.SetMasterClient(PhotonNetwork.LocalPlayer);
    }
}
