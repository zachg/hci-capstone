using Mapbox.Unity.Map;
using Photon.Pun;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ChooseWord : MonoBehaviour
{
    [SerializeField] private Transform scrollableListGridContent;
    [SerializeField] private GameObject wordButtonPrefab;
    private TMPro.TMP_Text[] wordButtonsText;
    private List<string> wordBank; // 20 words
    private List<string> wordsSent; // 20 words if we have 4 players and numWordsChosen = 5


    private ActorList<string> chosenWords; // list of words chosen by each player
    private PhotonView photonView; // needed to do RPCs

    private ActorList<bool> playersReady; // a list of actors and boolean pairs to mark who is ready
    private ActorList<bool> playersFinished; // a list of actors and boolean pairs to mark who is finished

    private Coroutine currentTimer; // current timer coroutine: countdown between each section of a phase

    private readonly int PreTimerPeriod = 1; // number of seconds for the pre choosing timer
    private int timerPeriod; // number of seconds for the choosing timer

    // pre timer window UI
    [SerializeField] private GameObject preChooseUI;

    [SerializeField] private GameObject chooseUI;
    [SerializeField] private TMPro.TMP_Text timerText; // timer text field

    [SerializeField] private GameObject waitingUI; // UI for waiting for all players to finsh choosing

    public Action OnFinished;

    // ---------------------------------------------------------------------------------------------------------------
    // Getter function:
    public ActorList<string> ChosenWords => chosenWords;

    private int numWordOptions; // the number of words that they player can choose from

    // Note: when a word is placed back into the list after not being chosen you could insert the word to a random index within the list

    // SetUp runs once when the scene starts and calls the NumWordOptions from the Room Settings. Stores numWordOptions once.
    public void SetUp(RoomOptionsData roomSettings)
    {
        photonView = GetComponent<PhotonView>();
        numWordOptions = roomSettings.NumWordOptions;
        timerPeriod = roomSettings.ChooseWordTime;
        chosenWords = new ActorList<string>(PhotonNetwork.PlayerList);
        CreateWordButtons();
        // randomize the order of the list otherwise everyone gets the same options on new game
        wordBank = new List<string>
        {
            "Cat",
            "Dog",
            "Frog",
            "Star",
            "Balloon",
            "Cherry",
            "Bow",
            "Ice Cream",
            "Bird",
            "Smiley",
            "Kite",
            "Fish",
            "Heart",
            "Stack",
            "Leaf",
            "Hat",
            "House",
            "Gem",
            "Hexagon",
            "Glasses",
            "Cloud",
            "Sun",
            "Chair",
            "Light Bulb",
            "Window",
            "Among Us",
            "Banana",
            "Flower",
            "Apple",
            "Pizza",
            "Phone",
            "Car",
            "Music Note",
            "Bunny",
            "Water",
            "Kirby",
            "Candle",
            "Cake",
            "Traffic Cone",
        };
        RandomizeWords(wordBank);
    }

    /// <summary>
    /// Fisher-Yates shuffle Algorithm referenced from: 
    /// https://www.geeksforgeeks.org/shuffle-a-given-array-using-fisher-yates-shuffle-algorithm/
    /// 
    /// </summary>
    /// <param name="wordList"> The list of words to be randomized</param>
    private void RandomizeWords(List<string> wordList)
    {
        System.Random randomObj = new System.Random();

        // Swap elements from the last element until the second element.
        // We don't need to swap the first element
        for (int i = wordList.Count - 1; i > 0; i--)
        {
            // Choose a random index: 
           int randomIndex = randomObj.Next(wordList.Count);

            // Swap the element at index i with the element at the random index:
            string tempWord = wordList[i];
            wordList[i] = wordList[randomIndex];
            wordList[randomIndex] = tempWord;
        }

        wordBank = wordList;
    }

    private void CreateWordButtons()
    {
        wordButtonsText = new TMPro.TMP_Text[numWordOptions]; // array of size numWordOptions

        for (int i = 0; i < numWordOptions; i++)
        {
            GameObject newWordButton = Instantiate(wordButtonPrefab, scrollableListGridContent); // makes the new gameobject a child of the scrollableListGridContent transform
            newWordButton.GetComponent<Button>().onClick.AddListener(ChoseWord);
            wordButtonsText[i] = newWordButton.transform.GetChild(0).GetComponent<TMPro.TMP_Text>(); // add one of the text components to the array
        }
    }

    // Sets things up before every phase
    public void Initialize()
    {
        timerText.text = timerPeriod.ToString();
        preChooseUI.SetActive(true); // turn on UI pre timer group

        if (PhotonNetwork.IsMasterClient)
        {
            if (playersReady == null) playersReady = new ActorList<bool>(PhotonNetwork.PlayerList);
            playersReady[PhotonNetwork.LocalPlayer.ActorNumber] = true;
            
            // check if all the players are ready
            foreach (ActorList<bool>.ActorData<bool> actorElem in playersReady)
            {
                if (!actorElem.data) return;
            }

            photonView.RPC("RPC_InitializeContinueChoose", RpcTarget.Others);
            currentTimer = StartCoroutine(PreTimer());

            playersFinished = new ActorList<bool>(PhotonNetwork.PlayerList);
            DistributeWordsToUsers();
        }
        else
        {
            photonView.RPC("RPC_InitializeChoose", RpcTarget.MasterClient, PhotonNetwork.LocalPlayer.ActorNumber);
        }
    }

    [PunRPC]
    private void RPC_InitializeChoose(int actorNumber)
    {
        if (playersReady == null) playersReady = new ActorList<bool>(PhotonNetwork.PlayerList);
        playersReady[actorNumber] = true;

        // check if all the players are ready
        foreach (ActorList<bool>.ActorData<bool> actorElem in playersReady)
        {
            if (!actorElem.data) return;
        }

        photonView.RPC("RPC_InitializeContinueChoose", RpcTarget.Others);
        currentTimer = StartCoroutine(PreTimer());

        playersFinished = new ActorList<bool>(PhotonNetwork.PlayerList);
        DistributeWordsToUsers();
    }

    [PunRPC]
    private void RPC_InitializeContinueChoose()
    {
        currentTimer = StartCoroutine(PreTimer());
        Debug.Log("pre timer RPC_InitializeContinueChoose");
    }

    /// <summary>
    /// Only run by Master Client
    /// </summary>
    private void DistributeWordsToUsers()
    {
        wordsSent = new List<string>();
        for (int i = 0; i < PhotonNetwork.PlayerListOthers.Length; i++)
        {
            string[] wordsForOneUser = new string[numWordOptions];
            for (int  j = 0; j < numWordOptions; j++)
            {
                wordsForOneUser[j] = wordBank[0];
                wordsSent.Add(wordBank[0]);
                wordBank.RemoveAt(0); // remove word from wordBank
            }
            if (numWordOptions > 1)
            {
                photonView.RPC("RPC_WordOptions1", PhotonNetwork.PlayerListOthers[i], wordsForOneUser);
            }
            else
            {
                photonView.RPC("RPC_WordOptions2", PhotonNetwork.PlayerListOthers[i], wordsForOneUser[0]);
            }
        }
        // Master client:
        for (int i = 0; i < numWordOptions; i++)
        {
            wordButtonsText[i].text = wordBank[0];
            wordsSent.Add(wordBank[0]);
            wordBank.RemoveAt(0); // remove word from wordBank
        }
    }

    [PunRPC]
    private void RPC_WordOptions1(string[] wordsForOneUser)
    {
        for (int i = 0; i < wordButtonsText.Length; i++)
        {
            wordButtonsText[i].text = wordsForOneUser[i];
        }
    }

    /// <summary>
    /// There is an error with trying to send a single string as a string array.
    /// </summary>
    /// <param name="wordForOneUser"></param>
    [PunRPC]
    private void RPC_WordOptions2(string wordForOneUser)
    {
        wordButtonsText[0].text = wordForOneUser;
    }

    public void ChoseWord()
    {
        string wordChosen = EventSystem.current.currentSelectedGameObject.transform.GetChild(0).GetComponent<TMPro.TMP_Text>().text; // check that this works
        Debug.Log($"Local player chose word: {wordChosen}");
        // Run the StopChoosing function and add the chosenword as the parameter
        StopChoosing(wordChosen);
    }

    /// <summary>
    /// The timer before the choosing phase starts.
    /// </summary>
    /// <returns>Coroutine waits 1 second intervals.</returns>
    private IEnumerator PreTimer()
    {
        for (int i = PreTimerPeriod; i > 0; i--)
        {
            yield return new WaitForSeconds(1f); // like sleep for 1 second
        }
        StartChoosing();
    }

    /// <summary>
    /// signals when the player can start choosing. turns off the pre choosing ui and turns on the choosing ui.
    /// </summary>
    private void StartChoosing()
    {
        currentTimer = StartCoroutine(Timer());

        preChooseUI.SetActive(false); // turn on UI pre timer group
        chooseUI.SetActive(true); // turn on UI choose group
    }


    /// <summary>
    /// Timer for the period of time the user has to choose their picture.
    /// </summary>
    /// <returns>Coroutine waits 1 second intervals.</returns>
    private IEnumerator Timer()
    {
        for (int i = timerPeriod; i > 0; i--)
        {
            timerText.text = i.ToString();
            yield return new WaitForSeconds(1f);
        }
        AutomaticallyChoseWord();
    }

    // User did not choose word in time, so we automatically choose word for them
    public void AutomaticallyChoseWord()
    {
        string wordChosenAuto = wordButtonsText[UnityEngine.Random.Range(0, numWordOptions)].text;
        Debug.Log($"Choose word timer ran out. Local player automatically chose word: {wordChosenAuto}");
        StopChoosing(wordChosenAuto);
    }

    /// <summary>
    /// can either be triggered by the timer running out or the done button being pressed.
    /// </summary>
    public void StopChoosing(string wordChosen)
    {
        if (currentTimer != null) StopCoroutine(currentTimer); // TODO may need to check null?

        // TODO show waiting screen in the meantime
        chooseUI.SetActive(false);
        waitingUI.SetActive(true);
        chosenWords[PhotonNetwork.LocalPlayer.ActorNumber] = wordChosen;

        if (PhotonNetwork.IsMasterClient)
        {
            wordsSent.Remove(wordChosen);

            // check the finished states of all the users
            playersFinished[PhotonNetwork.LocalPlayer.ActorNumber] = true;

            foreach (ActorList<bool>.ActorData<bool> actorElem in playersFinished)
            {
                if (!actorElem.data) return;
            }

            // tell all other clients that this game state is over
            photonView.RPC("RPC_FinishChoose", RpcTarget.Others);
            for (int i = 0; i < wordsSent.Count; i++)
            {
                wordBank.Insert(UnityEngine.Random.Range(0, wordBank.Count), wordsSent[i]);
            }
            wordsSent = null;
            playersReady = null;
            playersFinished = null;
            waitingUI.SetActive(false); // turn off the waiting UI
            OnFinished(); // action (a specific type of event) that one can subscribe to, every phase has this action
        }
        else
        {
            // tell the master client to set your actornumber to finished
            photonView.RPC("RPC_StopChoosing", RpcTarget.MasterClient, PhotonNetwork.LocalPlayer.ActorNumber, wordChosen);
        }
    }

    /// <summary>
    /// Tells the master client that another client has finished choosing.
    /// This function will check if all players are finished.
    /// Run by the master client.
    /// </summary>
    /// <param name="actorNumber">The photon actor number of the client that is finished.</param>
    [PunRPC]
    private void RPC_StopChoosing(int actorNumber, string wordChosen)
    {
        chosenWords[actorNumber] = wordChosen;
        wordsSent.Remove(wordChosen);
        Debug.Log($"Player with actorNumber {actorNumber} chose word: {wordChosen}");

        // check the finished states of all the users
        playersFinished[actorNumber] = true;

        foreach (ActorList<bool>.ActorData<bool> actorElem in playersFinished)
        {
            if (!actorElem.data) return;
        }

        // tell all other clients that this game state is over
        photonView.RPC("RPC_FinishChoose", RpcTarget.Others);
        for (int i = 0; i < wordsSent.Count; i++)
        {
            wordBank.Insert(UnityEngine.Random.Range(0, wordBank.Count), wordsSent[i]);
        }
        wordsSent = null;
        playersReady = null;
        playersFinished = null;
        waitingUI.SetActive(false); // turn off the waiting UI
        OnFinished(); // action (a specific type of event) that one can subscribe to, every phase has this action
    }

    /// <summary>
    /// Call the OnFinished action. Run by the non master clients.
    /// </summary>
    [PunRPC]
    private void RPC_FinishChoose()
    {
        waitingUI.SetActive(false); // turn off the waiting UI
        OnFinished();
    }
}