﻿using Mapbox.Unity.Map;
using Photon.Pun;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

// https://stackoverflow.com/questions/44264468/convert-rendertexture-to-texture2d

public class SendReceiveImage : MonoBehaviour
{
    private PhotonView myPhotonView; // allows us to send data to other clients through remote procedure calls
    [SerializeField] private RawImage myImage;

    private void Start()
    {
        myPhotonView = GetComponent<PhotonView>();
    }

    /*
     * Fields and functions used by the sending client.
     */

    /// <summary>
    /// Function to send an image. Starts coroutine.
    /// </summary>
    public void SendImage()
    {
        StartCoroutine(SendImageBuffered((Texture2D)myImage.texture, 300, 0.05f));
    }

    /// <summary>
    /// Sends an image to all other clients in the room in byte chunks specified by a number of segments. 
    /// Chunks are sent with a specified time between remote procedure calls.
    /// </summary>
    /// <param name="image">The image you would like to send.</param>
    /// <param name="chunks">The number of byte chunks you would like to split the image into.</param>
    /// <param name="timeBetweenRPCs">The time between a remote procedure call that sends a segment.</param>
    /// <returns>Coroutine method.</returns>
    private IEnumerator SendImageBuffered(Texture2D image, int chunks, float timeBetweenRPCs)
    {
        byte[] data = image.GetRawTextureData();

        myPhotonView.RPC("RPC_StartReceiveImage", RpcTarget.Others, data.Length, image.width, image.height, (int)image.format, chunks); // tell other clients to start receiving image

        int lengthRemaining = data.Length;
        for (int i = 0; i < chunks; i++)
        {
            int length = Mathf.Min(data.Length / chunks, lengthRemaining); // if at the tail end of the array use the value that is remaining
            lengthRemaining -= data.Length / chunks; // subtracting a segment each time;

            byte[] imageChunk = new byte[length];

            for (int a = 0; a < length; a++)
            {
                imageChunk[a] = data[((data.Length / chunks) * i) + a];
            }

            myPhotonView.RPC("RPC_ReceiveImageSegment", RpcTarget.Others, imageChunk, i);

            yield return new WaitForSeconds(timeBetweenRPCs);
        }
    }

    /*
     * Fields and functions used by the receiving client.
     */

    private Texture2D bufferedTexture;
    private int textureChunks;
    private byte[] textureData;

    /// <summary>
    /// Function run by receiving client containing necessary information to begin receiving an image.
    /// </summary>
    /// <param name="length">Total length of the byte array used to create the image.</param>
    /// <param name="width">Width of the image.</param>
    /// <param name="height">Height of the image.</param>
    /// <param name="format">Texture2D format of the image.</param>
    /// <param name="chunks">Number of chunks the image is split into.</param>
    [PunRPC]
    private void RPC_StartReceiveImage(int length, int width, int height, int format, int chunks)
    {
        bufferedTexture = new Texture2D(width, height, (TextureFormat)format, false);
        textureChunks = chunks;
        textureData = new byte[length];
    }

    /// <summary>
    /// Function run by receiving client when a chunk of bytes for an image is received.
    /// </summary>
    /// <param name="imageChunk">A chunk of bytes for an image.</param>
    /// <param name="chunkIndex">The index of the chunk of bytes.</param>
    [PunRPC]
    private void RPC_ReceiveImageSegment(byte[] imageChunk, int chunkIndex)
    {
        int startPos = (textureData.Length / textureChunks) * chunkIndex;
        for (int i = 0; i < imageChunk.Length; i++)
        {
            textureData[startPos + i] = imageChunk[i];
        }

        if (chunkIndex == textureChunks - 1)
        {
            bufferedTexture.LoadRawTextureData(textureData);
            bufferedTexture.Apply();
            myImage.texture = bufferedTexture;
        }
    }
}

public class ImageGuess : MonoBehaviour
{
    [SerializeField] private PhotonView myPhotonView; // allows us to send data to other clients through remote procedure calls
    
    [SerializeField] private GPSDraw gpsDraw;
    [SerializeField] private Draw draw;

    private int roundNum = 1;

    private int guessedActorNumber;
    [SerializeField] private RawImage guessedDrawing;

    private Texture2D bufferedTexture;
    private int textureChunks;
    private byte[] textureData;

    [SerializeField] private AbstractMap map;

    private double drawingLatitude;
    private double drawingLongitude;

    // TODO reminder that the text chat must be cleared between rounds

    /// <summary>
    /// Checks if it is the local client's turn to send their drawing to the other clients.
    /// </summary>
    private void CheckTurn()
    {
        int numActorNumbersLower = 0;
        for (int i = 0; i < PhotonNetwork.PlayerList.Length; i++) // getting playerlist is faster despite doing an unnessary loop
        {
            if (PhotonNetwork.PlayerListOthers[i].ActorNumber < PhotonNetwork.LocalPlayer.ActorNumber)
            {
                numActorNumbersLower++;
            }
        }

        if (numActorNumbersLower == roundNum - 1) // TODO change this depending if round num starts at 1 or 0
        {
            guessedActorNumber = PhotonNetwork.LocalPlayer.ActorNumber;
            SendImage(); // send the image to the other players
        }
    }

    /// <summary>
    /// Function to send an image. Starts coroutine.
    /// </summary>
    public void SendImage()
    {
        StartCoroutine(SendImageBuffered(RenderTextureToTexture2D(draw.DrawingRenderTexture), 300, 0.05f));
    }

    /// <summary>
    /// Converts the current image on a RenderTexture to a Texture2D.
    /// Original Source: https://stackoverflow.com/questions/44264468/convert-rendertexture-to-texture2d
    /// </summary>
    /// <param name="renderTexture">The RenderTexture you would like to copy.</param>
    /// <returns>The image converted to Texture2D.</returns>
    private Texture2D RenderTextureToTexture2D(RenderTexture renderTexture)
    {
        Texture2D texture2D = new Texture2D(renderTexture.width, renderTexture.height, TextureFormat.ARGB32, false);
        // ReadPixels looks at the active RenderTexture.
        RenderTexture.active = renderTexture;
        texture2D.ReadPixels(new Rect(0, 0, renderTexture.width, renderTexture.height), 0, 0);
        texture2D.Apply();
        return texture2D;
    }

    /// <summary>
    /// Function run by receiving client containing necessary information to begin receiving an image.
    /// </summary>
    /// <param name="actorNumber">The actorNumber of the sending client.</param>
    /// <param name="latitude">Latitude of the image on the map.</param>
    /// <param name="longitude">Longitude of the image on the map.</param>
    /// <param name="length">Total length of the byte array used to create the image.</param>
    /// <param name="width">Width of the image.</param>
    /// <param name="height">Height of the image.</param>
    /// <param name="chunks">Number of chunks the image is split into.</param>
    [PunRPC]
    private void RPC_StartReceiveImage(int actorNumber, double latitude, double longitude, 
        int length, int width, int height, int chunks)
    {
        guessedActorNumber = actorNumber;
        drawingLatitude = latitude;
        drawingLongitude = longitude;
        bufferedTexture = new Texture2D(width, height, TextureFormat.ARGB32, false);
        textureChunks = chunks;
        textureData = new byte[length];
    }

    /// <summary>
    /// Shows the image. If image being guessed is the local client's, then show the render texture.
    /// </summary>
    private void ShowImage()
    {
        if (PhotonNetwork.LocalPlayer.ActorNumber == guessedActorNumber)
        {
            // show map
            draw.gameObject.SetActive(true);
            // center the camera over this position
        }
        else
        {
            // may need to add loading in case the full image has not yet been received

            // show raw image
            Vector3 mapPosition = map.GeoToWorldPosition(new Mapbox.Utils.Vector2d(drawingLatitude, drawingLongitude));
            guessedDrawing.transform.parent.parent.position = new Vector3(mapPosition.x, 0.5f, mapPosition.z);
            guessedDrawing.transform.parent.parent.gameObject.SetActive(true);
            // center the camera over this position
        }
    }

    /// <summary>
    /// Hides the image. Resets the render texture for the client who sent the drawing.
    /// </summary>
    private void HideImage()
    {
        if (guessedActorNumber == PhotonNetwork.LocalPlayer.ActorNumber)
        {
            // hide map and reset the drawing on the canvas
            draw.ResetCanvas();
            draw.gameObject.SetActive(false);
        }
        else
        {
            // hide raw image and get rid of it
            guessedDrawing.texture = null;
            guessedDrawing.transform.parent.parent.gameObject.SetActive(false);
        }
    }


    /// <summary>
    /// Sends an image to all other clients in the room in byte chunks specified by a number of segments. 
    /// Chunks are sent with a specified time between remote procedure calls.
    /// </summary>
    /// <param name="image">The image you would like to send.</param>
    /// <param name="chunks">The number of byte chunks you would like to split the image into.</param>
    /// <param name="timeBetweenRPCs">The time between a remote procedure call that sends a segment.</param>
    /// <returns>Coroutine method.</returns>
    private IEnumerator SendImageBuffered(Texture2D image, int chunks, float timeBetweenRPCs)
    {
        int[] data = OptimizeDrawing(image);

        myPhotonView.RPC("RPC_StartReceiveImage", RpcTarget.Others,
            PhotonNetwork.LocalPlayer.ActorNumber, gpsDraw.CanvasLatitude, gpsDraw.CanvasLongitude,
            data.Length, image.width, image.height, chunks); // tell other clients to start receiving image

        int lengthRemaining = data.Length;
        for (int i = 0; i < chunks; i++)
        {
            int length = Mathf.Min(data.Length / chunks, lengthRemaining); // if at the tail end of the array use the value that is remaining
            lengthRemaining -= data.Length / chunks; // subtracting a segment each time;

            int[] imageChunk = new int[length];

            for (int a = 0; a < length; a++)
            {
                imageChunk[a] = data[((data.Length / chunks) * i) + a];
            }

            myPhotonView.RPC("RPC_ReceiveImageSegment", RpcTarget.Others, imageChunk, i);

            yield return new WaitForSeconds(timeBetweenRPCs);
        }
    }

    /// <summary>
    /// Optimizes the amount of data contained in a drawn image by returning an int 
    /// array containing only data for colored pixels. This reduces the amount of data 
    /// to be sent over the internet.
    /// </summary>
    /// <param name="image">The image you would like to optimize.</param>
    /// <returns>An int array where each array element represents a position and a color.</returns>
    private int[] OptimizeDrawing(Texture2D image)
    {
        int count = 0;
        byte[] rawData = image.GetRawTextureData();
        int[] unreducedData = new int[rawData.Length / 4];

        for (int i = 0; i < unreducedData.Length; i++)
        {
            if (rawData[i * 4] != 0) // ignore empty space
            {
                if (rawData[(i * 4) + 1] == 255) // red
                {
                    unreducedData[count] = (i * 4) << 4;
                }
                else if (rawData[(i * 4) + 2] == 255) // green
                {
                    unreducedData[count] = ((i * 4) << 4) + 1;
                }
                count++;
            }
        }

        int[] data = new int[count];

        for (int i = 0; i < count; i++)
        {
            data[i] = unreducedData[i];
        }
        return data;
    }


    /// <summary>
    /// Function run by receiving client when a chunk of ints for an image is received.
    /// </summary>
    /// <param name="imageChunk">A chunk of ints that contain the position of the color in an image.</param>
    /// <param name="chunkIndex">The index of the chunk of bytes.</param>
    [PunRPC]
    private void RPC_ReceiveImageSegment(int[] imageChunk, int chunkIndex)
    {
        // Reconstruct the drawing from the chunk
        for (int i = 0; i < imageChunk.Length; i++)
        {
            switch (imageChunk[i] & 0x0F)
            {
                case 0: // red
                    textureData[imageChunk[i] >> 4] = 255;
                    textureData[(imageChunk[i] >> 4) + 1] = 255;
                    break;
                case 1: // green
                    textureData[imageChunk[i] >> 4] = 255;
                    textureData[(imageChunk[i] >> 4) + 2] = 255;
                    break;
            }
        }

        if (chunkIndex == textureChunks - 1)
        {
            // finished getting all of the chunks
            bufferedTexture.LoadRawTextureData(textureData);
            bufferedTexture.Apply();
            textureData = null;
            guessedDrawing.texture = bufferedTexture;
        }
    }
}