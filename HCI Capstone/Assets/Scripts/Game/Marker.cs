using System;
using UnityEngine;
using UnityEngine.UI;

public class Marker : MonoBehaviour
{
    [SerializeField] private Transform marker;
    [SerializeField] private Toggle toggle;
    [SerializeField] private GameObject icon;

    public Action<Marker, bool> OnSelect;

    public Transform MarkerTransform => marker;
    public Toggle Toggle => toggle;
    public GameObject Icon => icon;

    public void Select(bool state)
    {
        OnSelect(this, state);
    }
}
