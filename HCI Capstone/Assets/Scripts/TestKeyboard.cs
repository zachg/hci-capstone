using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestKeyboard : MonoBehaviour
{
    [SerializeField] private TMPro.TMP_Text text;
    [SerializeField] private TMPro.TMP_InputField inputField;
    private TouchScreenKeyboard keyboard;
    private bool isOpen;

    // Start is called before the first frame update
    void Start()
    {
        TouchScreenKeyboard.hideInput = true;
        inputField.onSelect.AddListener((arg) => Open());
        inputField.onDeselect.AddListener((arg) => Close());
    }

    public void Open()
    {
        isOpen = true;
        
        keyboard = TouchScreenKeyboard.Open("", TouchScreenKeyboardType.ASCIICapable, false, false);
    }

    public void Close()
    {
        isOpen = false;
        keyboard.active = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (inputField.isFocused)
        {

        }
    }

    public static int GetRelativeKeyboardHeight(RectTransform rectTransform, bool includeInput)
    {
        int keyboardHeight = GetKeyboardHeight(includeInput);
        float screenToRectRatio = Screen.height / rectTransform.rect.height;
        float keyboardHeightRelativeToRect = keyboardHeight / screenToRectRatio;

        return (int)keyboardHeightRelativeToRect;
    }

    // https://forum.unity.com/threads/keyboard-height.291038/
    private static int GetKeyboardHeight(bool includeInput)
    {
#if UNITY_EDITOR
        return 0;
#elif UNITY_2017_1_OR_NEWER
        return 0;
#elif UNITY_ANDROID
        using (AndroidJavaClass unityClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
        {
            AndroidJavaObject unityPlayer = unityClass.GetStatic<AndroidJavaObject>("currentActivity").Get<AndroidJavaObject>("mUnityPlayer");
            AndroidJavaObject view = unityPlayer.Call<AndroidJavaObject>("getView");
            AndroidJavaObject dialog = unityPlayer.Get<AndroidJavaObject>("mSoftInputDialog");
            if (view == null || dialog == null)
                return 0;
            var decorHeight = 0;
            if (includeInput)
            {
                AndroidJavaObject decorView = dialog.Call<AndroidJavaObject>("getWindow").Call<AndroidJavaObject>("getDecorView");
                if (decorView != null)
                    decorHeight = decorView.Call<int>("getHeight");
            }
            using (AndroidJavaObject rect = new AndroidJavaObject("android.graphics.Rect"))
            {
                view.Call("getWindowVisibleDisplayFrame", rect);
                return Screen.height - rect.Call<int>("height") + decorHeight;
            }
        }
#elif UNITY_IOS
        return (int)TouchScreenKeyboard.area.height;
#endif
    }
}
