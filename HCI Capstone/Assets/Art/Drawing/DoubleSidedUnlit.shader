Shader "Unlit/DoubleSidedUnlit" {
    Properties {
        _Color ("Color", Color) = (1,1,1)
    }
 
    SubShader {
        Cull off

        Color [_Color]
        Pass {}
    }
}